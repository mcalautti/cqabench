OVERVIEW
========
All our datasets can be retrieved from the following link

[https://bit.ly/2X9ax1d](https://bit.ly/2X9ax1d)

In particular, due to space limitations, we do not provide the full databases, but only the result of our preprocessing step, for each pair (D,Q) of a database and query we considered in our experimental evaluation. That is, for each (D,Q), we provide a tar.gz archive, containing the encodings of all synopses of D. Each tar.gz file can be given as input to one of our approximation algorithms, which will compute an approximation of the relative frequency, for each synopsis in the archive.


The structure of our datasets is presented below.


ORGANIZATION
============
There are two main archives in our dataset

* test_scenarios.tar
* validation_scenarios.tar

## Test Scenarios
In the archive test_scenarios.tar we collect all queries, and corresponding synopses, of all Test Scenarios we considered in the paper.


We recall that we considered, for each number of joins j in 1,2,3,4,5, five base queries. The output variables of such queries is then modified, depending on the level of balance we need (e.g., for boolean queries, i.e., zero balance, we consider no output variables).

As an example, the following tar.gz archive (in test_scenarios.tar)
<pre>
synopses/<b>bool</b>/<b>20</b>/<b>4</b>/q4b_<b>2</b>.tar.gz
</pre>
contains all the synopses of the TPC-H database obtained by adding **20%** of noise, w.r.t. to the **2nd** query of our base batch of five queries with **4** joins, when considering no output variables, i.e., the **boolean** version of the query. 

The json file (in test_scenarios.tar)
<pre>
queries/bool/<b>20</b>/<b>4</b>/q4.json
</pre>

contains all our boolean queries with **4** joins, with additional configuration for generating databases with **20%** noise.

Considering non-boolean queries (in test_scenarios.tar), as an example, each tar.gz archive in the directory

<pre>
synopses/non-bool/<b>20</b>/<b>3</b>/q3_<b>2</b>/
</pre>

contains all the synopses of the TPC-H database obtained by adding **20%** of noise, w.r.t. to the **2nd** query of our base batch of five querieses with **3** joins, where the output variables have been chosen to obtain a specific amount of balance. For example, the archive q3_2_661.tar.gz therein contains the synopses relative to the 2nd query of the 3-joins batch, where the output variables have been modified in such a way that the number of output tuples is 661 (which, for this query, corresponds to 0.1 balance).

The json file

<pre>
queries/non-bool/<b>20</b>/<b>3</b>/q3_<b>2</b>/q3_<b>2</b>.json
</pre>
collects all queries, based on the **2nd** query of our **3**-joins batch, each with a different set of output variables. Each such query corresponds to one .tar.gz archive in the directory test_scenarios/synopses/non-bool/**20**/**3**/q3_**2**/

## Validation Scenarios (TPC-H and TPC-DS)

The archive validation_scenarios.tar, contains two directories "tpch" and tpcds", where in each we collect all queries, and corresponding synopses, of all Validation Scenarios we considered in the paper, for the corresponding benchmark.

As an example, the tar.gz archive

<pre>
tpch/synopses/<b>20</b>/q<b>4</b>.tar.gz
</pre>
contains all the synopses of the TPC-H database obtained by adding **20%** of noise, w.r.t. to the conjunctive query obtained from the one with identifier **4**, in the query workload of TPC-H.

The json file

<pre>
tpch/queries/q-tpch-bench-<b>20</b>.json
</pre>
collects all the conjunctive queries we obtained from the TPC-H query workload, with additional configuration for generating an inconsistent database with noise **20%**.



