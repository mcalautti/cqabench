#!/usr/bin/env python2

import json
import argparse
import sys

from moz_sql_parser import parse
from cqarewriting import queryRewriter
from cqarewriting import constants

def getExperimentName(queryName, args):
	if args.expschema_override:
		return args.expschema_override
	
	prefix = "experiment"
	if args.prefix:
		prefix = args.prefix
	return prefix+"_"+queryName+"_"+args.noisePerc+"_"+args.minBlockSize+"_"+args.maxBlockSize

parser = argparse.ArgumentParser()
parser.add_argument('queryFilePath', help='Query file path')
parser.add_argument('outFilePath', help='Output file path')
parser.add_argument('schemaName', help='Schema Name')
parser.add_argument('noisePerc', help='Noise percentage')
parser.add_argument('minBlockSize', help='Minimum block size')
parser.add_argument('maxBlockSize', help='Maximum block size')
parser.add_argument('-forceRewrite', help='Force Rewriting',  action="store_true")
parser.add_argument('-prefix', help='Prefix for experiment names')
parser.add_argument('-expschema_override', help='Forces the name of the experiment schemas')


args = parser.parse_args()

#Load files
queryJSON = {}
with open(args.queryFilePath) as json_file:
		    queryJSON = json.load(json_file)


batch = {}
batch["name"] = queryJSON["name"]
batch["queries"] = []
batch["experiments"] = []

print "Preparing query JSON...."

for q in queryJSON["queries"]:
	parsedQuery = parse(q["original"])
	if  isinstance(parsedQuery["from"], basestring):
		parsedQuery["from"] = [parsedQuery["from"]]
	tabs = set()
	for t in parsedQuery["from"]:
		if isinstance(t, basestring):
			tabs.add(t)
		elif isinstance(t, dict):
			tabs.add(t["value"])
	noiseParams = []

	for t in tabs:
		numberOfBlocks = args.noisePerc
		noiseParams.append({'name' : t, 'numberOfBlocks' : numberOfBlocks, 'blockSizeMin' : args.minBlockSize, 'blockSizeMax' : args.maxBlockSize, 'isPerc' : 'true'})

	e = {"name" : getExperimentName(q["name"], args), "query" : q["name"], "schema" : args.schemaName, "noiseParams" : noiseParams}
	
	batch["queries"].append(q)
	batch["experiments"].append(e)

if(args.forceRewrite):
	print "Rewriting queries...."
	for query in batch["queries"]:
		query["rew"] = queryRewriter.rewrite(query["original"], constants.VIEW_SUFFIX)


print "Writing file..."
with open(args.outFilePath, "w") as outfile:  
    json.dump(batch, outfile, indent = 5)

print "Done!"