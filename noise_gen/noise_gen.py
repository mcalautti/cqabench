#!/usr/bin/env python2

import psycopg2
import json
import sys 
import gc
import random
import math
import argparse
import time
import io
import csv

from cqarewriting import connectionBuilder
from cqarewriting import constants
from cqarewriting import viewsGenerator
from cqarewriting import queryRewriter

def getSecond(tup):
	return tup[1]

def setKey(keyAttributes, tup, keyPositions):
		newTup = []
		j = 0
		for i in range(len(tup)):
			if i in keyPositions:
				newTup.append(keyAttributes[j])
				j += 1
			else:
				newTup.append(tup[i])
		return newTup

def getTableName(tab):
	split = tab.split('.',1)
	if(len(split)>1):
		return split[1]
	else:
		return split[0]

parser = argparse.ArgumentParser()

parser.add_argument('connectionFile', help='Connection file path')

parser.add_argument('schemaFile', help='Schema file path')

parser.add_argument('queryFile', help='Query file path')

parser.add_argument('--startFrom', dest='startFrom', help='Name of the first experiment to consider.')
parser.add_argument('--bufferSize', dest='bufferSize', type=int, help='Buffer size value for the buffered reader')
parser.add_argument('--inMemory', dest='inMemory', action="store_true", help='Use memory instead of files to cache violation tuples')

args = parser.parse_args()


if args.startFrom != None:
	print ("Starting from experiment: "+ args.startFrom)

dbConn = connectionBuilder.dbConnection(args.connectionFile)

#Load files
schemaJSON = {}
with open(args.schemaFile) as json_file:
		    schemaJSON = json.load(json_file)
queryJSON = {}
with open(args.queryFile) as json_file:
		    queryJSON = json.load(json_file)

loadedTables = {}
loadedColumnStrings = {}
loadedKeyPositions = {}

#Flag to take track of starting experiment.
hasStarted = False

#Noise generation starts here!
for exp in queryJSON['experiments']:

	gc.collect()

	tzero = int(round(time.time() * 1000))

	print("Experiment: " + exp["name"])
	if(args.startFrom == None or args.startFrom == exp["name"]):
		hasStarted = True

	if (hasStarted == False):
		print("Not starting yet. Skipping...")
		continue

	if "skip" in exp and exp["skip"] == True:
		print("\tSkipping..")
		continue

	#Select schema name for the new tables.
	cursor = dbConn.conn.cursor()
	newSchemaName = exp['schema'] + '_' + exp['name']
	if 'experiment_schema' in exp:
		newSchemaName = exp['experiment_schema']
		print("Overriding experiment schema name: "+newSchemaName)
    
	print("\tCreating schema: " + newSchemaName)
	cursor.execute('CREATE SCHEMA IF NOT EXISTS ' + newSchemaName)
	cursor.close()

	#Load query for current experiment.
	q = ''
	for query in queryJSON['queries']:
		if query['name'] == exp['query']:
			q = query

	if (q == ''):
		print('Cannot find query for experiment ' + exp['name'])
		continue

	#Execute rewritten query over the database.
	cursor = dbConn.conn.cursor()
	cursor.execute('SET search_path = ' + exp['schema'])		
	print("\tEvaluating query...")
	cursor.execute(queryRewriter.rewrite(q["original"], constants.VIEW_SUFFIX))
	
	#Search for the beginning of metadata.		
	firstMetadataPosition = -1
	for i, col in enumerate(cursor.description):
		if(col.name == '_table'):
			firstMetadataPosition = i
			break
	if(firstMetadataPosition == -1):
		print("No metadata in the answer")
		exit()

	numberOfTables = (len(cursor.description) - firstMetadataPosition)/4
		
	#Load BlockIDs (BIDs) from query result
	bids = {}
	for t in cursor:
		for i in range(0, numberOfTables):
			tabName = getTableName(t[firstMetadataPosition+(4*i)])
			bid = t[firstMetadataPosition+(4*i)+1]
			bidSet = set()
			if tabName in bids:
				bidSet = bids[tabName]
			bidSet.add(bid)
			bids[tabName] = bidSet
	
	if len(bids) == 0:
		print("\tQuery returned the empty set. Skipping...")
		continue

	#Garbage collection. Only BIDs are kept in memory.
	print("\tGarbage collection for query result.")
	cursor.close()
	gc.collect()

	#Noise injection starts here!			
	for param in exp['noiseParams']:
		currentTableName = param['name']
		print("\tAdding noise to " + currentTableName)

		tabBids = bids[currentTableName]
		tabSchema = {}
		for tab in schemaJSON['tables']:
			if tab['name'] == currentTableName:
				tabSchema = tab
				break
		if tabSchema=={}:
			print('Table ' + currentTableName + ' is not in the schema.')
			exit()

		cursor = dbConn.conn.cursor()

		print("\t\tLoading table...")
		
		#Execute query
		cursor.execute('SET search_path = ' + exp['schema'])		
		cursor.execute('SELECT * FROM ' + currentTableName + constants.VIEW_SUFFIX + ' ORDER BY _bid')
		
		#Compute key positions from attribute names
		keyPositions = []
		attList = []
		for i, col in enumerate(cursor.description):
			attList.append(col.name)
			for kAtt in tabSchema['key']:
				if(col.name == kAtt):
					keyPositions.append(i)

		#Load number of blocks value from the JSON file
		#If nbVal represents a percentage, the number of
		#blocks is computed accordingly
		nbVal = int(param['numberOfBlocks'])
		availableBlocks = 0

		if ('isPerc' in param) and param['isPerc'] == "true":
			availableBlocks = int(math.ceil(len(tabBids)*(float(nbVal)/100)))
			print("\t\tNumber of blocks: " + str(nbVal) + "%. Value: " + str(availableBlocks))
		else:
			availableBlocks = min(cursor.rowcount-1,nbVal)
			print("\t\tNumber of blocks: " + str(nbVal) + ". Available: " + str(availableBlocks))

		selectedKeys = []
		selectedTups = []

		if (availableBlocks == 0):
			print("\t\tNumber of blocks is zero. Only the table will copied.")
		else:
			#Sample the list of bids
			print("\t\tSampling the list of bids...")
			randomTabBids = random.sample(tabBids,availableBlocks)
			tabBids = None


			print("\t\tLoading keys in memory...")
			#Load keys in memory and randomize blocks
			blockSizeMin =  int(param['blockSizeMin'])
			blockSizeMax =  int(param['blockSizeMax'])
			blockSizeMax = min(blockSizeMax, cursor.rowcount)
			if blockSizeMin > blockSizeMax:
				blockSizeMin = blockSizeMax

			randomTabBids.sort()
			
			currentTupleIndex = -1
			for b in randomTabBids:
				tup = cursor.fetchone()
				currentTupleIndex += 1
				
				while tup[-3] != b:
					tup = cursor.fetchone()
					currentTupleIndex += 1
				
				keyToAdd = []

				for kPos in keyPositions:
					keyToAdd.append(tup[kPos])
				selectedKeys.append(keyToAdd)
				
				#Randomize block size, taking into account
				#the tuple already in the database for the
				#given key (hence the -1)
				blockSize = random.randint(max(0, blockSizeMin-1), max(0, blockSizeMax-1))
				selTupsToAdd = []
				for i in range(blockSize):
					pairToAdd = [len(selectedKeys)-1, -1]
					pairToAdd[1] = random.randint(0, cursor.rowcount-1)
					while (pairToAdd[1] == currentTupleIndex) or (pairToAdd in selTupsToAdd):
						pairToAdd[1] = random.randint(0, cursor.rowcount-1)
					selTupsToAdd.append(pairToAdd)
				selectedTups.extend(selTupsToAdd)	
			selectedTups.sort(key = getSecond)
			
		#Create new table		
		print("\t\tCopying table...")
		cursorInsert = dbConn.conn.cursor()
		cursorInsert.execute('CREATE TABLE ' + newSchemaName + "." + currentTableName + " AS SELECT * FROM " + currentTableName)
		dbConn.conn.commit()
		#cursorInsert.execute('SET search_path = ' + newSchemaName)
		
		#Insert tuples in the new table  
		print("\t\tInserting new tuples...")
		cursor.scroll(0, 'absolute')
		currentSelectedTup = None
		if availableBlocks == 0:
			continue

		currentTupleIndex = -1
		lenSelTup = len(selectedTups)
		print("\t\t\tWriting file...")
		
		cache_table = None
		if(args.inMemory):
			cache_table = io.BytesIO()
		else:
			cache_table = io.FileIO("_tmp.csv", 'w+')	
		
		#buffer_size is in bytes (apparently)
		buffSiz = io.DEFAULT_BUFFER_SIZE
		if args.bufferSize != None:
			buffSiz = args.bufferSize

		bw = io.BufferedWriter(cache_table, buffer_size=buffSiz)
		csv_w = csv.writer(bw)
		
		while len(selectedTups) != 0:
			currentSelectedTup = selectedTups.pop(0)
			while currentTupleIndex < currentSelectedTup[1]:
				currentTuple = cursor.fetchone()	
				currentTupleIndex +=1
			tupToAdd = setKey(selectedKeys[currentSelectedTup[0]], currentTuple, keyPositions) 
			csv_w.writerow(tupToAdd[:-4])

		#Flush and rewind. 
		bw.flush()
		cache_table.seek(0)
		print("\t\t\tCopying tuples...")

		cursorInsert.copy_expert("COPY " + newSchemaName + "." + currentTableName +" FROM STDIN WITH CSV DELIMITER ',' QUOTE '\"'", cache_table)
		dbConn.conn.commit()

		#Explicit garbage collection for the current table.
		print("\tGarbage collection for table.")
		bw.close()
		cache_table.close()
		cache_table=None
		keyPositions = None
		selectedKeys = None
		selectedTups = None
		cursor.close()
		cursor = None
		cursorInsert.close()
		cursorInsert = None
		gc.collect()

	#Make views
	print("\tDefining views....")
	viewGeneratorInstance = viewsGenerator.viewsGenerator(schemaJSON)
	cursorInsert = dbConn.conn.cursor()
	for param in exp['noiseParams']:
		tab_name = param['name']
		q = viewGeneratorInstance.getView(tab_name, newSchemaName)
		cursorInsert.execute("CREATE VIEW " + newSchemaName +"." +tab_name + constants.VIEW_SUFFIX + " AS " +q)
	dbConn.conn.commit()
	tone = int(round(time.time() * 1000))
	print("\tNoise generation done in " + str(tone - tzero) + "ms")
dbConn.conn.close()