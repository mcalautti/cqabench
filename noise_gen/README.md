[![License: GPL3.0+](https://img.shields.io/badge/LICENSE-GPL3.0%2B-yellow.svg)](https://www.gnu.org/licenses/)

OVERVIEW
========

This directory contains the following scripts:

set_noise_parameters.py
=
Arguments:
* json query file (src)
* json query file with noise settings (trgt)
* schema name
* noise percentage
* minblock
* maxblock

Description: Attaches to each query in the src file the given noise, minblock and maxblock parameters, and saves the result to trgt. The noisy database will be constructed from the one in the schema "schema name".  
The resulting file is likely going to be used by the noise generator to generate noise for each query, according to the parameters.

noise_gen.py
=
Arguments:
* json connection file
* json schema file
* json file with queries (each with their noise parameters, see above)
 
Description: For each query in the json file, creates a new schema in the database, named over the query and noise parameters, and creates an inconsistent database in that schema for that query, with the corresponding noise parameters.



REQUIREMENTS (pip):
=============
[moz_sql_parser 2.44.19084](https://github.com/mozilla/moz-sql-parser)  
[psycopg2 2.8.3](http://initd.org/psycopg/)  

HOW TO USE
==============
Running the scripts without arguments will show an help.
