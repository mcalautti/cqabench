import itertools, codecs, re, sys, json

class Predicate:

    def __init__(self,symbol,arity,keyAttributes):
        self.symbol = symbol
        self.arity = arity
        self.key = keyAttributes
        self.key.sort()
    
        self.nonKey = [i for i in range(arity) if i not in self.key]

    def __eq__(self,other):
        return self.symbol == other.symbol and self.arity == other.arity
    
    def __str__(self):
        return self.symbol+"/"+str(self.arity)

    def __hash__(self):
        return str(self).__hash__()

class Atom:

    def __init__(self,pred,terms):
        self.pred = pred
        if pred.arity != len(terms):
            raise Exception("Number of terms is wrong")
        
        self.terms = terms

        self.keyTerms = [t for i,t in enumerate(terms) if i in pred.key]
        self.nonKeyTerms = [t for i,t in enumerate(terms) if i in pred.nonKey]

        self.keyVars = list(set([t for t in self.keyTerms if t >= 0]))
        self.nonKeyVars = list(set([t for t in self.nonKeyTerms if t >= 0]))
        self.vars = list(set([t for t in terms if t >= 0]))

    def __eq__(self,other):
        return self.pred == other.pred and self.terms == other.terms
    
    def __str__(self):
        return str(self.pred)+str(self.terms)

    def __hash__(self):
        return str(self).__hash__()

class Query:

    def __init__(self,atoms):
        self.atoms = list(set(atoms))

        self.liaison = []
        temp = []
        for a in atoms:
            for v in a.vars:
                temp.append(v)

        self.vars = list(set(temp))

        self.liaison = list(set([v for v in temp if temp.count(v) > 1]))

        self.complexPart = []

        temp = set()
        for a in atoms:
            for v in a.nonKeyTerms:
                if v in self.liaison or v < 0:
                    temp.add(a)
                    break
        
        self.complexPart = list(temp)

        self.varsInAllComplexPartKVars = []

        temp = set()

        if len(self.complexPart) > 0:
            temp = set(self.complexPart[0].keyVars)
        
        for i in range(len(self.complexPart)-1):
            temp = temp.intersection(self.complexPart[i+1].keyVars)
        
        self.varsInAllComplexPartKVars = list(temp)

    def __str__(self):
        return ",".join(map(str,self.atoms))

def replaceVarWithConst(q,v):
    newAtoms = []
    for a in q.atoms:
        pred = a.pred
        newTerms = a.terms.copy()
        for i in range(len(newTerms)):
            if newTerms[i] == v:
                newTerms[i] = -1
        
        newAtoms.append(Atom(pred,newTerms))
    
    return Query(newAtoms)
    
def parseQuery(qFile, schema, asBoolean):

    regex = r"([A-Za-z_0-9]+)\((.*)\)"

    with codecs.open(qFile,"r","utf-8") as f:
        strQ = f.read().splitlines()

    headStr = strQ[0]

    matches = re.findall(regex,headStr)
    outVars = matches[0][1].split(',')

    bodyQ = strQ[1:]

    atoms = []
    varMap = {}
    varId = 0
    for atom in bodyQ:
        matches = re.findall(regex,atom)
        predStr = matches[0][0]
        termsStr = matches[0][1].split(',')

        for info in schema['tables']:
            if info['name'] == predStr:
                arity = len(info['attributes'])
                keyAttr = []
                for k in info['key']:
                    keyAttr.append(info['attributes'].index(k))
                break

        pred = Predicate(predStr,arity,keyAttr)
        if len(termsStr) != arity:
            raise Exception("Ambigous atom: "+atom)

        terms = []
        for t in termsStr:
            if t[0] == '?':
                if t in outVars and not asBoolean:
                    terms.append(-1)
                elif t not in varMap:
                    varMap[t] = varId
                    terms.append(varId)
                    varId = varId + 1
                else:
                    terms.append(varMap[t])
            else:
                terms.append(-1)
        
        atoms.append(Atom(pred,terms))
    
    return Query(atoms)
    
def isSafe(q):

    #SE0a
    if len(q.atoms) == 1 and len(q.atoms[0].vars) == 0:
        print("SE0a: "+str(q))
        return True
    
    #SE0b
    if len(q.complexPart) == 0:
        print("SE0b: "+str(q))
        return True

    #SE1
    for perm in itertools.permutations(q.atoms):
        for i in range(len(perm)):
            q1 = Query(perm[0:i])
            q2 = Query(perm[i:])
            if len(q1.atoms) != 0 and len(q2.atoms) != 0 and len(set(q1.vars).intersection(set(q2.vars))) == 0:
                print("SE1: "+str(q1)+" - "+str(q2))
                return isSafe(q1) and isSafe(q2)
    
    #SE2
    if len(q.complexPart) != 0 and len(q.varsInAllComplexPartKVars) > 0:
        v = q.varsInAllComplexPartKVars[0]
        newQuery = replaceVarWithConst(q,v)
        print("SE2: "+str(newQuery))
        return isSafe(newQuery)

    #SE3
    for a in q.atoms:
        if len(a.keyVars) == 0 and len(a.vars) > 0:
            v = a.vars[0]
            newQuery = replaceVarWithConst(q,v)
            print("SE3: "+str(newQuery))
            return isSafe(newQuery)
    
    print("Fail")
    return False

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: query.txt schema.json")
        sys.exit(0)

    with codecs.open(sys.argv[2],"r","utf-8") as f:
        schema = json.load(f)

    q = parseQuery(sys.argv[1],schema,False)

    print(str(q))

    if isSafe(q):
        print("Safe")
    else:
        print("Not Safe")

