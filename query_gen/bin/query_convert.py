import os, json, codecs, sys, re, qsafety, math

def getQueryFileName(out_dir,qIds,id):
    #digits = int(math.log10(len(qIds)))+1
    strId = str(id).zfill(3)
    return out_dir+"/"+"query"+strId+"/"+"q"+str(id)+".txt"

if len(sys.argv) != 5 and len(sys.argv) != 6:
    print("Usage: query_name out_dir schema.json treat_as_boolean (0/1) input_log_file/stdin")
    sys.exit(0)


if len(sys.argv) == 6:
    with codecs.open(sys.argv[5],"r","utf-8") as f:
        data = f.read()
else:
    data = sys.stdin.read()

asBoolean = False
if int(sys.argv[4]) == 1:
    asBoolean = True

regex = r"(SELECT DISTINCT .*\nFROM.*\nWHERE.*\n(\t AND.*\n)*)\n\n.* - \|q([0-9]+)\|=([0-9]+)\n.* - DIR created"

matches = re.findall(regex,data)

queries = [q[0].replace("DISTINCT ","").replace("\n"," ").replace("\t ","").strip() for q in matches]

if asBoolean:
    queries = [re.sub(r"SELECT .* FROM ","SELECT * FROM ",q) for q in queries]

qIds = [int(q[2]) for q in matches]
qSizes = [ int(q[3]) for q in matches]

i = 1
out_dir = sys.argv[2]
query_name = sys.argv[1]
schemaFile = sys.argv[3]

print(qSizes)

with codecs.open(schemaFile,"r","utf-8") as f:
    schema = json.load(f)

json_str = {"queries":[], "name":query_name}
for q in queries:
    entry = {"original":q, "name":query_name+"_"+str(i)}
    #exp_entry = {"query":query_name+"_"+str(i), "name":"exp_"+query_name+"_"+str(i), "schema":"public", "experiment_schema": "public" }

    qName = getQueryFileName(out_dir,qIds,i)
    isSafe = 'unk'
    if os.path.exists(qName):
    	Q = qsafety.parseQuery(qName,schema,asBoolean)
    	isSafe = qsafety.isSafe(Q)

    entry['isSafe']=isSafe
    json_str["queries"].append(entry)

    #json_str["experiments"].append(exp_entry)
    i = i + 1

with codecs.open(out_dir+"/"+query_name+".json","w","utf-8") as f:
    json.dump(json_str,f,indent=4)
