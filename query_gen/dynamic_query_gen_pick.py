#!/usr/bin/env python3

import random, sys, os, json, codecs, argparse, time, psycopg2, csv, hashlib
from moz_sql_parser import parse
from moz_sql_parser import format
from cqarewriting import queryRewriter
from cqarewriting import constants
from cqarewriting import connectionBuilder

parser = argparse.ArgumentParser()
parser.add_argument("input_query",help="json file with queries")
parser.add_argument("query_name",help="name of the query")
parser.add_argument("output_query",help="output json file with new queries")


args = parser.parse_args()

with codecs.open(args.input_query,'r',"utf-8") as json_file:
	queryJSON = json.load(json_file)

queries = queryJSON['queries']
queries = [x for x in queries if x['name'].startswith(args.query_name)]
experiments = {}
for exp in queryJSON['experiments']:
	if exp['query'].startswith(args.query_name):
		experiments[exp['query']] = exp

queries.sort(key=lambda x: x['count'])

maxTuples = queries[-1]['count']
minTuples = queries[0]['count']

rangeSize = (maxTuples-minTuples)//10

if rangeSize==0:
	print('Not enough queries for generating batch. Exiting...')
	sys.exit(0)

k = rangeSize//2

newJSON = {}
newJSON['name'] = args.query_name
newJSON['queries'] = []
newJSON['experiments'] = []

while k<maxTuples:
	print('Analyzing range: '+str(k))
	minDiff = abs(queries[0]['count']-k)
	minQuery = queries[0]
	for q in queries:
		diff = abs(q['count']- k)
		if diff < minDiff:
			minDiff = diff
			minQuery = q

	temp = dict(minQuery)
	temp['name'] = args.query_name+"_"+str(temp['count'])
	print('Query selected for range '+str(k)+' is: '+temp['name'])

	newExp = dict(experiments[minQuery['name']])
	newExp['query'] = temp['name']

	newJSON['queries'].append(temp)
	newJSON['experiments'].append(newExp)
	k = k + rangeSize

with codecs.open(args.output_query,'w','utf-8') as outf:
	json.dump(newJSON, outf, indent = 5)

