#!/usr/bin/env python3

import random, sys, os, json, codecs, argparse, time, psycopg2, csv, hashlib
from moz_sql_parser import parse
from moz_sql_parser import format
from cqarewriting import queryRewriter
from cqarewriting import constants
from cqarewriting import connectionBuilder

def runQuery(qname,qStr,cursor,baseSchema,exp_name):
	schemaName = baseSchema+'_'+exp_name
	cursor.execute('SET search_path = '+schemaName)

	cursor.execute(qStr)
	return cursor.fetchone()[0]

parser = argparse.ArgumentParser()
parser.add_argument("input_query",help="json file with queries")
parser.add_argument("output_query",help="output json file with new queries")
parser.add_argument("output_log",help="output log file with new queries infos")
parser.add_argument('connection',help="connection.json")
parser.add_argument("perc",type=float,help="max percentage of attributes to select")
parser.add_argument("timeout",type=float,help="timeout in seconds")

args = parser.parse_args()

with codecs.open(args.input_query,'r',"utf-8") as json_file:
	queryJSON = json.load(json_file)

k = 1
queries = queryJSON['queries']
jsonExperiments = queryJSON['experiments']

dbConn = connectionBuilder.dbConnection(args.connection)

homImages = {}
cursor = dbConn.conn.cursor()

experiments = {}
for exp in jsonExperiments:
	experiments[exp['query']] = exp
	
for q in queries:
	qStr = parse(q['original'])
	qStr['select'] = {"value":{"COUNT":"*"}}
	homImages[q['name']]=runQuery(q['name'],format(qStr),cursor,experiments[q['name']]['schema'],experiments[q['name']]['name'])

startTime = time.time()
csvData = []

newJSON = {}
newJSON['name'] = queryJSON['name']
newJSON['queries'] = []
newJSON['experiments'] = []
hashes = set()

csvFile = codecs.open(args.output_log,'w','utf-8')
csvWriter = csv.writer(csvFile)

while True:
	endTime = time.time()

	if endTime-startTime>args.timeout:
		print('Timedout')
		break

	temp = random.choice(queries)
	q = dict(temp)

	oldname = q['name']
	q['name'] = q['name']+'_'+str(k)

	parsedQuery = parse(q["original"])
	if parsedQuery['select'] == '*':
		print('* is unsupported')
		sys.exit(-1)

	numAttr = len(parsedQuery['select'])
	minNumAttr = 1 #int(round(minPerc * numAttr))
	maxNumAttr = int(round(args.perc*numAttr))

	if maxNumAttr < minNumAttr:
            maxNumAttr = minNumAttr

	chosenNum = random.randint(minNumAttr,maxNumAttr)
	chosenList = sorted(random.sample(range(numAttr),chosenNum))
	newSelect = [parsedQuery['select'][i] for i in chosenList]

	attrList = [newSelect[i]['value'] for i in range(len(newSelect))]

	queryIdentifier = oldname+str(attrList)
	digest = hashlib.sha1(queryIdentifier.encode('utf-8')).digest()

	if digest in hashes:
		print('Repeated query found, skipping...')
		continue
	
	hashes.add(digest)
	parsedQuery['select'] = newSelect

	q['original'] = format(parsedQuery)

	parsedQuery['select'] = {"value":{"COUNT":{"DISTINCT":attrList}}}
	countQueryStr = format(parsedQuery)

	count = runQuery(oldname,countQueryStr,cursor,experiments[oldname]['schema'],experiments[oldname]['name'])

	if count > 1 and count < homImages[oldname]:
		print('Digest: '+str(digest))
		print(countQueryStr)
		print(count)
		q['count'] = count
		q['hom_images'] = homImages[oldname]

		newJSON['queries'].append(q)
		newExp = dict(experiments[oldname])
		newExp['query'] = q['name']
		newJSON['experiments'].append(newExp)
		
		csvWriter.writerow([oldname,q['name'],chosenNum,numAttr,count,homImages[oldname]])
		csvFile.flush()

		with codecs.open(args.output_query,'w','utf-8') as outf:
			json.dump(newJSON, outf, indent = 5)

		k = k + 1

cursor.close()
csvFile.close()
