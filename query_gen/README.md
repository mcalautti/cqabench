[![License: GPL3.0+](https://img.shields.io/badge/LICENSE-GPL3.0%2B-yellow.svg)](https://www.gnu.org/licenses/)

OVERVIEW
========

This directory contains the following scripts:

static_query_gen.sh
=
Arguments:
* connection.properties file
* stats.xml (values for each attribute)
* schema.xml (database schema)
* query-polycies.xml (number of joins and output variables)
* outdir
* query name (base query name for generated queries)
* schema.json (database schema in json)
* boolean flag (specifies if the queries should be constructed as boolean, regardless of the specified number of output variables)

Description: This is a wrapper script to the static query generator presented in \[1\]. Generated queries are stored in json format in the given outdir.  
Example input files to this script can be found in the `static_query_gen_example` directory.

dynamic_query_gen_collect.py
=
Arguments:
* json file with queries (src)
* new json file with queries (trgt)
* out log file (log)
* connection file in json
* output variables percentage (attrPerc)
* timeout

Description: For each query q in src, it repeatedly generates queries with the body of q and a random subset of the output variables of q, which are ar most attrPerc% of the total. Each resulting query is tested against the database having the given noise minBlock, and maxBlock parameters specified for for that query in the json file. The balance of each generate query is recorded in the output json file.    
All the constructed queries are stored in the trgt json file, until the given timeout expires. Statistics on the execution are saved in the given log file.

dynamic_query_gen_pick.py
=
Arguments:
* json file with queries (src)
* query name (base name of queries to consider in the json)
* output json file with selected queries (trgt)

Description: The src json file is likely the one obtained after executing the dynamic_query_gen_collect.py above, containing a pool of queries with different balances. The tool picks, among the queries in src with name starting with the given query name, 10 queries, each with a balance as close as possible to 0.1,0.2,...,1.0, respectively.  
The result is stored in the trgt file.

rewrite_queries.py
=
Arguments:
* original queries json file (src)
* rewritten queries json file (rew)

Description: Adds to each query in the src file, the corresponding rewriting, and saves the result to rew.


REQUIREMENTS (pip):
=============
[moz_sql_parser 2.44.19084](https://github.com/mozilla/moz-sql-parser)  
[psycopg2 2.8.3](http://initd.org/psycopg/)  

HOW TO USE
==============
Running the scripts without arguments will show an help.

REFERENCES
==============
\[1\]: Michael Benedikt, George Konstantinidis, Giansalvatore Mecca, Boris Motik, Paolo Papotti, Donatello Santoro, Efthymia Tsamoura:
Benchmarking the Chase, PODS 2017.