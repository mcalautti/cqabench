#!/bin/sh

if [ "$#" -ne 8 ]
then
    echo "Usage: connection.properties stats.xml schema.xml query-policies.xml out_dir query_name schema.json treat_as_boolean (0/1)"
    exit
fi

java -classpath "bin/semi-automatic.jar:bin/lib/*" uk.ac.ox.cs.pdq.workloadrun.SemiAutomaticMode -c "$1" -a "$2" -s "$3" -q "$4" -o "$5/" | python3 bin/query_convert.py "$6" "$5" "$7" "$8"
