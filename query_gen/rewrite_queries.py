#!/usr/bin/env python2

from cqarewriting import constants
from cqarewriting import queryRewriter
from cqarewriting import viewsGenerator
import sys
import json

if (len(sys.argv) != 3):
	print("USAGE: rewriteQueries.py INPUT-FILE OUTPUT-FILE")
	exit() 

inputJSON = {}
with open(sys.argv[1]) as json_file:  
		    inputJSON = json.load(json_file)

for query in inputJSON["queries"]:
	query["rew"] = queryRewriter.rewrite(query["original"], constants.VIEW_SUFFIX)

with open(sys.argv[2], 'w') as json_file:  
		    inputJSON = json.dump(inputJSON, json_file, indent = 5)