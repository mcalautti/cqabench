OVERVIEW
========
This repository contains a collection of tools for benchmarking query answering over inconsistent databases. In particular, benchmarking approximation algorithms computing the relative frequency of a tuple. It also contains the datasets used in our paper.

The repository contains different sub-projects. Each sub-project comes with its own readme, describing its content and basic usage. The sub-projects are:

* _scenarios: provides a link to all our scenarios and a description of their structure
* algorithms: The approximation algorithms
* data_gen: the main tool for generating tpch/tpcds consistent databases
* noise_gen: tools for injecting noise in a given database, according to a specific query, and noise parameters
* query_gen: tools for randomly generating queries with desired parameters
* synopsis_gen: tool that constructs all the (query) synopses of a given database for all tuples with non-zero frequency. Synopses are encoded as particular DNF formulas, called positive Block DNF

All the software projects above have been designed for a Linux environment.

CLONING
=======
This repository has submodules, so it is suggested to clone like:  
`git clone --recurse-submodules https://gitlab.com/mcalautti/cqabench.git`  

HOW TO USE
========
The following describes a full workflow from generating the databases to running the approximation algorithms.
All python scripts below are assumed have been made executable with `chmod +x script.py`.

## Data Generation (data_gen)
Assuming config.json is a configuration file specifying data generation settings, the following command will generate all files to be imported in PostgreSQL:  
`tpc_gen.py config.json`  
For details on how to setup a configuration file, see the data_gen project.
The above command also generates a json file containing the schema of the generated database. Let it be `schema.json`.

## Static Query Generation (query_gen)
At this point, we need to generate random base queries. We do this with the command  
`static_query_gen.sh connection.properties stats.xml schema.xml query-policies.xml out_dir query_name schema.json 0`  
where `connection.properties` specifies how to connect to the database, `stats.xml` contains the admissible values for each attribute in the database, `schema.xml` is the schema of the database in xml format, `query-policies.xml` specifies the number of joins, of output variables and how many queries to generate, `out_dir` is the output directory where to store the generated queries (it must exist), `query_name` gives a name to the batch of queries, `schema.json` is the schema of the database in json format (obtained in the Data Generation phase). The last parameter is 0, unless the generated queries need to be made boolean, before saving them to file (in which case the parameter is 1).  
In `out_dir` a json file is created, call it `base_queries.json`, where queries are stored, each with their name.

Example input files to the above command can be found the query_gen project.

## Noise Generation (noise_gen)
Using the database and the queries generated above, we now perform *query-aware noise injection*. First, we need to specify, for the queries in `base_queries.json` the kind of noise we want. For example:  
`set_noise_parameters.py base_queries.json noise_queries.json schemaName 20 2 5`  
The file `base_queries.json` is the collection of queries generated with the static query generator, `noise_queries.json` is the file where the queries are saved, each with the given noise parameters, `schemaName` is the name of the schema (e.g., "public") where the (consistent) database resides (obtained in the Data Generation phase), 20 means 20% of noise, 2 and 5 means that each block of conflicting facts will have a random size between 2 and 5.

We can now use the obtained `noise_queries.json` file to generate, for each base query in there, an inconsistent database, with the associated noise parameters. For example:  
`noise_gen.py connection.json schema.json noise_queries.json`  
The above command will connect to the database, as specified in the connection.json file, and generate inconsistent databases according to the schema and queries given.

An example of a `connection.json` file can be found in the noise_gen project.

## Dynamic Query Generation (query_gen)
Having inconsistent databases at hand, we can now generate queries, based off the base ones in `noise_queries.json`, with different levels of balance. First, we generate a pool of candidate queries. For example:  
`dynamic_query_gen_collect.py noise_queries.json pool.json log.txt connection.json 0.5 7200`  
The above command constructs, for 2 hours (7200 seconds), a pool of queries in `pool.json`, obtained from queries in `noise_queries.json`, where each query in `pool.json` has its balance attached, w.r.t. to the corresponding noisy database specified in the `noise_queries.json` file. Moreover, the queries in `pool.json` are obtained by selecting a random subset of the output variables of the queries in `noise_queries.json`, of size at most 50% (0.5) of the total number of output variables.

We now select from `pool.json`, the queries with balance closer to 0.1,0.2,...,1.0. That is:  
`dynamic_query_gen_pick.py pool.json query_name final_queries.json`  
The file `final_queries.json` contains one query from `pool.json` for each balance in 0.1,0.2,...,1.0, picked from the ones with base name `query_name`.

## Rewriting Queries (query_gen)
We now need to rewrite each query in `final_queries.json`, in order to be able to construct the synopses. For this, we do:  
`rewrite_queries.py final_queries.json rew_queries.json`  
The file `rew_queries.json` contains all info present in `final_queries.json` plus the rewritings.

## Synopsis Construction (synopsis_gen)
We now construct the set of all synopses for each query in `rew_queries.json`. That is:  
`dnfRewriter connection.json rew_queries.json out_dir`  
The directory `out_dir` (must exists) will contain one .tar.gz file for each query in `rew_queries.json`, containing all the synopses of the noisy database corresponding to that query.
Statistics on the construction are written to stderr. So, one can collect statistics like so:  
`dnfRewriter connection.json rew_queries.json out_dir 2> log.txt`

## Running Approximation Algorithms (algorithms)
Each .tar.gz can be now given to an approximation algorithm. For example:  
`BlockDNFKL 0.1 0.25 file.tar.gz 0 3600`  
runs the approximation algorithm based on algorithm KL with epsilon=0.1, delta=0.25, over the synopses in `file.tar.gz`. This run is given id `0` (this is simply an id the user can specify that will be written in the log), and the algorithm signals timeout if execution time goes beyond 1 hour (3600).  
The full log of execution (i.e., running time, parameters and approximated values) is written to stderr. So, one can use:  
`BlockDNFKL 0.1 0.25 file.tar.gz 0 3600 2> log.txt`  
to redirect statistics to `log.txt`.