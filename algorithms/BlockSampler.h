#ifndef BLOCK_SAMPLER_H
#define BLOCK_SAMPLER_H

#include "BlockDNFFormula.h"
#include <cmath>
#include <string>

class BlockSampler
{
public:
	BlockSampler(BlockDNFFormula& F_, double _timeStart, double _timeout) : F(F_), timeStart(_timeStart), timeout(_timeout){}
	
	/**
	 * @brief Returns a vector of variables, one for each block.
	 * At position i in the result is the chosen variable in the block with id i.
	 * @return 
	 */
	virtual vector<int64_t> randomAssignment() =0;
	virtual std::string calculateFraction(double_t num, uint64_t den) = 0;
    bool timedout() { return hasTimedout; }
	protected:
		BlockDNFFormula& F;
        bool hasTimedout;
        double timeStart;
        double timeout;
};
#endif
