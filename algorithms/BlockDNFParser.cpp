#include "BlockDNFParser.h"

#include <regex>
#include <fstream>

#include "BlockDNFFormulaBuilder.h"

enum{
	PRS_HEADER,
	PRS_BLOCKS_CONTENT,
	PRS_CLAUSES,
	PRS_SUCCESS
};

bool BlockDNFParser::parse(const string& bdnf_path, BlockDNFFormula& F) throw(){
    vector<string> com;
    return parse(bdnf_path,F,com);
}
	
bool BlockDNFParser::parse(const string& bdnf_path, BlockDNFFormula& F, vector<string>& comments) throw(){
    ifstream inputFile;
	inputFile.open(bdnf_path);
	
	if(!inputFile)
		return false;
    
	auto result = parse(inputFile,F,comments);
    inputFile.close();
    return result;
}


bool BlockDNFParser::parse(istream& in_stream, BlockDNFFormula& F) throw(){
    vector<string> com;
    return parse(in_stream,F,com);
}

bool BlockDNFParser::parse(istream& inputFile, BlockDNFFormula& F, vector<string>& comments) throw(){

    comments.clear();
	BlockDNFFormulaBuilder builder(F);
	builder.setDebugMode(debugMode);
    
	if(!inputFile)
		return false;

	string line;
	int i = 0, n = 0, m = 0;
	
	std::regex header_rgx(R"(p\scnf\s(\d+)\s(\d+))");
	std::regex bmetadata_rgx(R"(c\s#begin_metadata)");
	std::regex emetadata_rgx(R"(c\s#end_metadata)");

	std::smatch header_match;
	std::regex bheader_rgx(R"(c\s#begin_blocks)");
	std::regex bfooter_rgx(R"(c\s#end_blocks)");
	int state=PRS_HEADER;

	while(getline(inputFile,line) && state!=PRS_SUCCESS){
		
		switch(state){
			
			case PRS_HEADER:
			{
				if(std::regex_match(line,bmetadata_rgx)){
					while(getline(inputFile,line)){
						if(std::regex_match(line,emetadata_rgx))
							break;
						
						if(line.length()<2 || line[0]!='c')
							return false;
						
						comments.push_back(line.substr(2,line.length()-2));
					}
				}
				
				if(std::regex_match(line,header_match,header_rgx) && header_match.size()>1){
					n=std::stoi(header_match[1].str());
					//the number of clauses for DNF purposes is less than the one declared.
					//We will properly correct this value at the end of the parsing procedure.
					m=std::stoi(header_match[2].str());
					state=PRS_CLAUSES;
				}else if(line.empty() || line[0]=='c')
					continue;
				else
					return false;
			}
			break;
			
			case PRS_CLAUSES:
			{
				if(line[0]=='c'){
					if(std::regex_match(line,bheader_rgx))
						state=PRS_BLOCKS_CONTENT;
					continue;
				}
					
				i++;
				if(i>m)
					return false;
				
				stringstream sline(line);
				int64_t lit;
				vector<int64_t> clause;
				while (sline>>lit && lit!=0){
					if(lit<0)
						return false;
				
					clause.push_back(lit);
				}
				
				builder.addClause(clause);
			}
			break;
			
			case PRS_BLOCKS_CONTENT:
			{
				//check if we are at the end of the block section
				if(std::regex_match(line,bfooter_rgx)){
					state=PRS_SUCCESS; //skip remaining clauses that are to be read only by a CNF counter
				}
				//if we have blocks to parse
				else if(line[0]=='c'){
					stringstream sline(line);
					int64_t lit;
					string com;
					if(sline>>com && com=="c"){
						//parse variables in the block
						vector<int64_t> block;
						while(sline>>lit && lit!=0){
							block.push_back(lit);
						}
						builder.addBlock(block);
					}else
						return false;
					
				}else
					return false;
			}
			break;
		}
	}
	
	if(state!=PRS_SUCCESS)
		return false;
	
	builder.build();
				
	return true;
}

static void writeBlockConstraint(ostream& outfile, const vector<int64_t>& block){
	//If block variables are x_1,...,x_n. Then we first need to guarantee that at least one variable in the block
	//is true. We should do this with the clause (x_1 v x_2 v ... v x_n), but since the dnf part is written with positive
	//occurrences of the variables, we have to invert it to (not x_1 v not x_2 ... v not x_n)
	for(size_t i=0;i<block.size();i++){
		auto curVar = block[i];
		outfile<<-curVar<<" ";
	}
	outfile<<"0"<<endl;
	
	//Now, we need to guarantee that no more than one variable is set to true. We should this by requiring, for every
	//pair x_i,x_j that (not x_i v not x_j), but as discussed above, we need to invert the variables, obtaining
	//(x_i v x_j).
	for(size_t i=0;i<block.size();i++){
		for(size_t j=i+1;j<block.size();j++){
			auto firstVar = block[i];
			auto secondVar = block[j];
			outfile<<firstVar<<" "<<secondVar<<" 0"<<endl;
		}
	}
}

bool BlockDNFParser::saveToFile(const string& out_path, const BlockDNFFormula& F, const vector<string>& comments){
    ofstream outfile(out_path);
    if(!outfile)
        return false;
    auto result = saveToStream(outfile,F,comments);
    outfile.close();
    return result;
}

bool BlockDNFParser::saveToFile(const string& out_path, const BlockDNFFormula& F, const std::initializer_list<string>& comments){
	vector<string> com = comments;
	return saveToFile(out_path,F,com);
}

bool BlockDNFParser::saveToFile(const string& out_path, const BlockDNFFormula& F, const string& comment){
	vector<string> com{comment};
	return saveToFile(out_path,F,com);
}

bool BlockDNFParser::saveToStream(ostream& outfile, const BlockDNFFormula& F, const std::initializer_list<string>& comments){
    vector<string> com = comments;
    return saveToStream(outfile,F,com);
}

bool BlockDNFParser::saveToStream(ostream& outfile, const BlockDNFFormula& F, const string& comment){
    vector<string> com{comment};
    return saveToStream(outfile,F,com);
}

bool BlockDNFParser::saveToStream(ostream& outfile, const BlockDNFFormula& F, const vector<string>& comments){

	if(!outfile){
		return false;
	}
	
	//write header
	//the number of total clauses of the cnf is the clauses of the DNF plus the clauses for the block constraints.
	//First, compute the total number of clauses of the cnf.
	size_t totalClauses=F.m;
	for(size_t i=0;i<F.blocks.size();i++){
		auto n = F.blocks[i].size();
		totalClauses = totalClauses + 1 + (n*(n-1))/2;
	}
	
	if(comments.size()>0){
		outfile<<"c #begin_metadata"<<endl;
		for(size_t i=0;i<comments.size();i++)
			outfile<<"c "<<comments[i]<<endl;
		outfile<<"c #end_metadata"<<endl;
	}
	
	outfile<<"p cnf "<<F.n<<" "<<totalClauses<<endl;
	
	//write DNF as is
	for(size_t i=1;i<=F.m;i++){
		
		auto& curClause = F.clauses[i];
		
		for(size_t j=0;j<curClause.size();j++){
			outfile<<curClause[j]<<" ";
		}
		outfile<<"0"<<endl;
	}
	
	//write custom block info
	outfile<<"c #begin_blocks"<<endl;
	for(size_t i=0;i<F.blocks.size();i++){
		outfile<<"c ";
		auto& curBlock = F.blocks[i];
		for(size_t j=0;j<curBlock.size();j++){
			outfile<<curBlock[j]<<" ";
		}
		outfile<<"0"<<endl;
	}
	outfile<<"c #end_blocks"<<endl;
	
	//write block constraints. Here, since we wrote the dnf part with positive variables, in other for the cnf
	//counter to treat the dnf part as the right cnf, we have to store the block constraints with all the variables inverted.
	outfile<<"c #begin_block_constraints"<<endl;
	for(size_t i=0;i<F.blocks.size();i++){
		writeBlockConstraint(outfile,F.blocks[i]);
	}
	outfile<<"c #end_block_constraints"<<endl;
	
	return true;
}

#ifdef MAIN_BLOCKDNFPARSER
int main(int argc, char* argv[]){
	BlockDNFParser parser;
	BlockDNFFormula F;
	vector<string> comments;
	parser.parse("q0_932.txt",F,comments);
	
	parser.saveToFile("out.bdnf",F,{"test1","test2"});
	return 0;
}
#endif 