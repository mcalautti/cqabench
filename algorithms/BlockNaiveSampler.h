#ifndef NAIVE_BLOCK_SAMPLER_H
#define NAIVE_BLOCK_SAMPLER_h

#include "BlockSampler.h"
#include "adcs/RandomBits.h"

#include "adcs/Timers.h"

class BlockNaiveSampler : public BlockSampler
{
public:
	BlockNaiveSampler(BlockDNFFormula& F_, double _timeStart, double _timeout) : BlockSampler(F_,_timeStart,_timeout) {
		rb.SeedEngine();
        double elapsedTime = cpuTimeTotal() - _timeStart;
        if(_timeout>0 && elapsedTime>_timeout)
            hasTimedout = true;
	}
	
	vector<int64_t> randomAssignment() override;
	string calculateFraction(double_t num, uint64_t den) override;
	
protected:
	RandomBits rb;
};

#endif