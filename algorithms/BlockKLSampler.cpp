#include "BlockKLSampler.h"

#include <iostream>
#include <vector>
#include <iomanip>
#include <algorithm>
#include <sstream>

using std::cout;
using std::endl;

static bool mpz_class_cmp(mpz_class a, mpz_class b){ return a<=b;} //not a<b since clauses are numbered 1 to m inclusive. lower_bound would return clause number 0 if not for a<=b

vector<int64_t> BlockKLSampler::randomAssignment(){
	//cout<<"1"<<endl;
	/*Generate a uniform random integer in the range 0 to U-1, inclusive. 
	*/
	mpz_urandomm(randNum_clause,state1, U);
	mpz_class x(randNum_clause);
	//cout<<"Finding lower bound"<<endl;
	
	/* lower_bound
		* Returns an iterator pointing to the first element in the range [first, last) 
		* that is not less than (i.e. greater or equal to) value, or last if no such element is found.
		*/ 
	// if x was 0 then pos would have been samp_array.begin() making low = 0. But clauses are numbered 1 to m.
	// instead of making x = x+1, we change definition of compare function to make a < b+1 return true which is equivalent to a<=b true.
	vector<mpz_class>::iterator pos = lower_bound(sampling_array.begin(),sampling_array.end(),x, mpz_class_cmp);
	//cout<<"Found lower bound"<<endl;
	int low = pos - sampling_array.begin();
	//randCls = low;
	/* Marco: here low represents the random clause picked with the right probability, taking into account the blocks. 
	 * Now, we need to sample a block assignment, i.e., one variable per block, where the variables of the clause are chosen.
	*/
	
	vector<int64_t> result = rb.GenerateRandomVariables(F.blocks);
	for(size_t i=0;i<F.clauses[low].size();i++){
		int64_t curVar = F.clauses[low][i];
		size_t varBlock = F.blocksByVar[curVar];
		result[varBlock]=curVar;
	}
	
	randClsNum = low;
	return(result);
}


std::string BlockKLSampler::calculateFraction(double_t num, uint64_t den){
	
	mpf_t fU,fnum,fden,ftotalAsnmt,fratio;
	
	int nBits, decimal_prec;
	decimal_prec = 15;
	nBits = 3.3* decimal_prec;
	nBits += nBits/2.0; //padding
	mpf_set_default_prec(nBits);
	
	mpf_init(fU);
	mpf_init(fnum);
	mpf_init(fden);
	mpf_init(ftotalAsnmt);
	mpf_init(fratio);
	
	mpf_set_z(fU,U);
	mpf_set_d(fnum,num);
	mpf_set_ui(fden,den);
	mpf_set_z(ftotalAsnmt,totalAsnmt);
	
	mpf_mul(fnum,fnum,fU);

	mpf_mul(fden,fden,ftotalAsnmt);
	mpf_div(fratio,fnum,fden);
	
	//gmp_printf("Final fraction is %.*Ff\n",decimal_prec, fratio);

	double_t fResult = mpf_get_d(fratio);

	//mpz_clear(U);
	//mpz_clear(totalAsnmt);
	mpf_clear(ftotalAsnmt);
	mpf_clear(fU);
	mpf_clear(fnum);
	mpf_clear(fden);
	mpf_clear(fratio);
	
    std::stringstream ss;
    ss<<fResult;
    
    std::string result = ss.str();
    
	return result;
}