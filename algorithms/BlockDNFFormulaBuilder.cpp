#include "BlockDNFFormulaBuilder.h"

#include <algorithm>
#include <exception>
#include <set>

static bool less_vectors(const vector<int64_t>& a,const vector<int64_t>& b) {
   return a.size() < b.size();
}

static bool more_vectors(const vector<int64_t>& a, const vector<int64_t>& b){
    return a.size() > b.size();
}

static bool less_clauses(const int64_t& a, const int64_t& b){
	return abs(a)<abs(b);
}

BlockDNFFormulaBuilder& BlockDNFFormulaBuilder::addClause(vector<int64_t>& clause) throw(){
    if(clause.size()==0)
        throw builder_exception("Empty clauses are not allowed!");

	sort(clause.begin(), clause.end(),less_clauses);
	
	if(clause[0]<1)
		throw builder_exception("Variable must be 1-indexed and positive");
	
	if(maxVarClauses<clause[clause.size()-1])
		maxVarClauses = clause[clause.size()-1];
    
    clausevars.insert(clause.begin(),clause.end());
	
    clauses_temp.push_back(clause);

	return *this;
}

BlockDNFFormulaBuilder& BlockDNFFormulaBuilder::addBlock(vector<int64_t>& block) throw(){
    if(block.size()==0)
        throw builder_exception("Block must be non-empty!");

    sort(block.begin(), block.end(),less_clauses);
    
    if(maxVarBlocks<block[block.size()-1])
		maxVarBlocks = block[block.size()-1];
    
    if(!debugMode){
        if(block.size()==1){
            varsSingletonBlock.push_back(block[0]);        
        }
    }
    
	vector<int64_t> curBlock;
	for(size_t i=0;i<block.size();i++){
		if(block[i]<=0)
			throw builder_exception("Variable must be 1-indexed and positive");
			
        //regardless of debug mode, we add the variables to blocksvars. This is needed to check for gaps.
		blocksvars.push_back(block[i]);
		if(std::find(curBlock.begin(),curBlock.end(),block[i]) != curBlock.end())
			throw builder_exception("Variables in block are not unique");

		curBlock.push_back(block[i]);
	}

    F.blocks.push_back(curBlock);

	return *this;
}

void BlockDNFFormulaBuilder::build() throw(){
	sort(blocksvars.begin(),blocksvars.end());
    sort(F.blocks.begin(),F.blocks.end(),more_vectors);

	if(noPartitionOrGap())
		throw builder_exception("Blocks are not a partition of the variables or they contain gaps in the variable names.");
	
	if(maxVarClauses>maxVarBlocks)
		throw builder_exception("The clauses contain a variable not mentioned by the blocks");
	
    if(!debugMode){
        optimize();
    }
    
	F.n=blocksvars.size();
	F.blocksByVar.resize(F.n+1);
	F.numBlockConstraints = 0;
    
	//construct blocksByVar data structure.
	for(size_t i=0;i<F.blocks.size();i++){
		auto& block = F.blocks[i];
		for(size_t j=0;j<block.size();j++){
			auto curVar = block[j];
			F.blocksByVar[curVar]=i;
		}
        F.numBlockConstraints += 1 + (block.size()*(block.size()-1))/2;
	}
    
	//remove conflicting clauses
	for(size_t i=0;i<clauses_temp.size();i++){
		auto& clause = clauses_temp[i];
		if(!isConflicting(clause))
			F.clauses.push_back(clause);
	}
	
	F.m=F.clauses.size()-1;
	
	sort(F.clauses.begin(),F.clauses.end(),less_vectors);
	F.minClauseSize = F.clauses[1].size();
	F.maxClauseSize = F.clauses[F.clauses.size()-1].size();
	
    F.minBlockSize = F.blocks[F.blocks.size()-1].size();
    F.maxBlockSize = F.blocks[0].size();
    
    //force the vector to be passed to a local one, so it will be destroyed at the end of this function, freeing memory.
	vector<vector<int64_t>>().swap(clauses_temp);
	unordered_set<int64_t>().swap(clausevars);
	vector<int64_t>().swap(blocksvars);
}

void BlockDNFFormulaBuilder::optimize(){
    sort(varsSingletonBlock.begin(),varsSingletonBlock.end());
    
    //at this point we know that blocksvars has no gaps, and thus, the new variable names, after removing the singleton ones
    //are the ones in the first blocksvars.size()-varsSingletonBlock.size() positions
    auto total = blocksvars.size();
    auto single = varsSingletonBlock.size();
    blocksvars.resize(total-single);
    
    //similarly, maxVarBlocks is easily adjusted
    maxVarBlocks = maxVarBlocks - single;
    
    //Now, we adapt the variable names in F.blocks.
    vector<vector<int64_t>> adaptedBlocks;
    
    for(size_t i = 0; i < F.blocks.size(); i++){
        auto& curBlock = F.blocks[i];
        if(curBlock.size()>1){
            
            for(size_t j=0;j<curBlock.size();j++){
                auto& curVar = curBlock[j];
                auto slope = getNumMissingVariablesBefore(curVar);
                if(slope<0)
                    throw builder_exception("Negative slope, this should never happer!");

                curVar = curVar - slope;
            }
            
            adaptedBlocks.push_back(curBlock);
        }else{
            break; //recall blocks are ordered from bigger to shortest
        }
    }
    
    //avoid a copy
    F.blocks=std::move(adaptedBlocks);
    
    //We do the same with the clauses. In this case, we need to both remove the missing variables, and adapt remaining variables names.
    //After removing the missing variables, the clause might be empty, but we still need to add it, as this is
    //denotes the fact that the formula is trivially satisfied. For compatibility with other tools, we simulate the empty clause,
    //with one having exactly one variable, that also makes its own block.
    std::set<vector<int64_t>> adaptedClauses;
    bool addedEmptyClause = false;
    for(size_t i=0;i<clauses_temp.size();i++){
        auto& curClause = clauses_temp[i];
        vector<int64_t> adaptedClause;
        
        for(size_t j=0;j<curClause.size();j++){
            auto& curVar = curClause[j];
            auto slope = getNumMissingVariablesBefore(curVar);
            //if the variable appears in a block of size greater than one, keep it and rename it.
            if(slope >= 0)
                adaptedClause.push_back((curVar-slope));
        }
            
        //avoid adding multiple empty clauses
        if(adaptedClause.size()==0 && !addedEmptyClause){
            //for compatibility with other tools, we add a single dummy clause, with a fresh variable in its own block
            maxVarBlocks++; maxVarClauses = maxVarBlocks;
            adaptedClause.push_back(maxVarBlocks);
            F.blocks.push_back({maxVarBlocks});
            blocksvars.push_back(maxVarBlocks);
            adaptedClauses.insert(adaptedClause);
            addedEmptyClause = true;
        }else if(adaptedClause.size()>0)
            adaptedClauses.insert(adaptedClause);
    } 
    
    clauses_temp.assign( adaptedClauses.begin(), adaptedClauses.end() );
    //avoid a copy
    //clauses_temp = std::move(adaptedClauses);
    
    //clean up temporary vector.
    vector<int64_t>().swap(varsSingletonBlock);
}

int BlockDNFFormulaBuilder::getNumMissingVariablesBefore(int64_t var){    
    size_t count = 0;
    size_t low = 0;
    size_t high = varsSingletonBlock.size();
    size_t pivot = 0;
    
    while(low < high){
        pivot = (high + low)/2;
        auto curVar = varsSingletonBlock[pivot];
        if(curVar<var){
            low = pivot + 1;
            count = pivot + 1;
        }else if(curVar>var)
            high = pivot;
        else
            return -1;
    }
    
    /*
    for(size_t i=0;i<varsSingletonBlock.size();i++){
        if(varsSingletonBlock[i]<var)
            count++;
        else
            return count; //remember that varsSingletonBlock is ordered
    }
    */
    
    return count;
}