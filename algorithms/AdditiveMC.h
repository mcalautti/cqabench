#ifndef ADDITIVE_MC_H
#define ADDITIVE_MC_H

#include <iostream>
#include "adcs/DKL.h"

class AdditiveMC{
	public:
		AdditiveMC(double_t eps_, double_t delta_, std::ostream& log_ = std::cout, double _timeStart=-1, double _timeout=-1):
		epsilon(eps_), delta(delta_), log_stream(log_), timeStart(timeStart), timeout(_timeout){}
		
		virtual int solve() = 0;		
	
	protected:
		int AdditiveAlgorithm();
			
		virtual double_t generateSample() = 0;
		
		double_t epsilon, delta;
		SampleCount count;
        
        std::ostream& log_stream;
        
        double timeStart;
        double timeout;
};

#endif