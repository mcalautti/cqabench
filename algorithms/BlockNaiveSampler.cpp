#include "BlockNaiveSampler.h"

#include <iostream>
#include <sstream>

using std::cout;
using std::endl;

vector<int64_t> BlockNaiveSampler::randomAssignment() {
	return rb.GenerateRandomVariables(F.blocks);
}

string BlockNaiveSampler::calculateFraction(double_t num, uint64_t den) {
	double_t fraction = num/den;
	
    std::stringstream ss;
    ss<<fraction;
    
    std::string result = ss.str();
	return result;
}