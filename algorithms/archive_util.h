#ifndef ARCHIVE_UTIL_H
#define ARCHIVE_UTIL_H

#include <archive.h>
#include <archive_entry.h>
#include <string>

using namespace std;

static archive* openMemoryArchive(const char* data, size_t size){
    archive *a = archive_read_new();
    archive_read_support_filter_gzip(a);
    archive_read_support_format_tar(a);
    if(archive_read_open_memory(a, data, size) != ARCHIVE_OK)
        return nullptr;
    
    return a;
}

static bool nextArchiveEntryString(archive* arc, string& outFileName, string& outString){
    archive_entry *entry;
    
    if(archive_read_next_header(arc, &entry) != ARCHIVE_OK)
        return false;
    
    outFileName.clear();
    outString.clear();
    
    outFileName.append(archive_entry_pathname(entry));
    
    ssize_t bufSize = archive_entry_size(entry);
        
    if(bufSize <= 0)
        bufSize = 1024*1024; //1MB buffer

    char* buff = new char[bufSize];
    outString.reserve(bufSize);
    
    while (true) {
        ssize_t readBytes = archive_read_data(arc, buff, bufSize);
        if (readBytes < 0) {
            delete[] buff;
            return false;
        }
        if (readBytes == 0){
            break;
        }
        outString.append(buff,readBytes);
    }
    
    delete[] buff;
    return true;
}
	
static void closeReadArchive(archive* arc){
	archive_read_close(arc);
 	archive_read_free(arc);
}

#endif

