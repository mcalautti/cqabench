#ifndef BLOCK_DNF_KLM_H
#define BLOCK_DNF_KLM_H

#include <vector>
#include <string>

#include "BlockKLSampler.h"
#include "BlockDNFParser.h"
#include "BlockDNFFormula.h"
#include "adcs/RandomBits.h"
#include "adcs/SelfAdjust.h"


class BlockDNFKLM : public SelfAdjust<std::vector<int64_t>>{
	public:
		BlockDNFKLM(BlockDNFFormula& F_, double_t eps_, double_t delta_, std::ostream& log_, double _timeStart, double _timeout):
		SelfAdjust(eps_,delta_, log_, _timeStart, _timeout), F(F_), DS(F_, _timeStart, _timeout){
			rb.SeedEngine();
		}

		int solve() override;
				
        std::string result;
        
	protected:
		
		std::vector<int64_t> generateSample() override;
		uint64_t generateIndex() override;
		uint64_t numberOfSets() override;
		bool satisfies(std::vector<int64_t> s, uint64_t index) override;
		
		BlockKLSampler DS;
		BlockDNFFormula& F;
		
		RandomBits rb;
		std::uniform_int_distribution<uint64_t> uid {1,F.m};
};

#endif