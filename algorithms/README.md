[![License: GPL3.0+](https://img.shields.io/badge/LICENSE-GPL3.0%2B-yellow.svg)](https://www.gnu.org/licenses/)

OVERVIEW
========

This is a *fork* of the Approximate Counting DNF Suite (ACDS) for DNF formulas by [Shrotri](https://gitlab.com/Shrotri/DNF_Counting).  
The goal of this fork is to extend the suite to *positive Block DNFs*, that is, DNF formulas with no negation, where variables are partitioned into blocks, and valid truth assignments are exclusively the ones obtained by selecting exactly one variable from each block.  
A positive Block DNF formula is essentially a space efficient encoding of the $(\Sigma,Q)$-synopsis of a database for some tuple.

The following are the implemented algorithms:

* BlockDNFDKLNaive: This is the algorithm "ApxCQA[Natural]", in our paper
* BlockDNFDKL: This is the algorithm "ApxCQA[KL]", in our paper
* BlockDNFDKLM: This is the algorithm "ApxCQA[KLM]", in our paper
* BlockDNFKLM: This is the algorithm "ApxCQA[Cover]", in our paper

REQUIREMENTS:
=============

[GNU Bignum 6.1.2](https://gmplib.org/)   
[libarchive 3.3.3](https://libarchive.org/)

INSTALLATION
==============

If the dependencies above are properly installed, running `make` will suffice to build the tools. 
All binaries will be found in the bin directory.

