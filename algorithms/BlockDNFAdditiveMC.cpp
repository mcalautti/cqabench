#include "BlockDNFAdditiveMC.h"
#include "BlockDNFParser.h"
#include <vector>
#include <sstream>
#include "adcs/Timers.h"
#include "CmdLineRunner.h"

using std::cout;
using std::cerr;
using std::endl;

#define TOOL_NAME "BlockDNFAdditiveMC"


double_t BlockDNFAdditiveMC::generateSample(){
	vector<int64_t> s = DS.randomAssignment();
	if (satisfies(s)){
		return 1;
	}
	return 0;
}

bool BlockDNFAdditiveMC::satisfies(vector<int64_t>& s){
	for(int i=1;i<=F.m;i++){
		bool sat = true;
		for(int j=0;j<F.clauses[i].size();j++){
			int64_t curVar = F.clauses[i][j];
			// Block DNF only has positive variables
			size_t block = F.blocksByVar[curVar];
			int64_t chosenVar = s[block];
			if(curVar != chosenVar) {
				sat = false;
				break;
			}
		}
		if(sat){
			return true;
		}
	}
	return false;
}

int BlockDNFAdditiveMC::solve(){
    if(DS.timedout())
        return -1;
	int retcode = AdditiveAlgorithm();
    if(retcode<0) return retcode;
	result = DS.calculateFraction(count.num, count.den);
    return 0;
}

#ifdef ENABLE_MAINS
int main(int argc, char* argv[]){
	return cmdlineRun<BlockDNFAdditiveMC>(argc,argv,TOOL_NAME);
}	
#endif