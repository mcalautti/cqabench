#include <vector>

#include "BlockDNFDKL.h"
#include "BlockDNFParser.h"
#include "adcs/Timers.h"
#include <sstream>

#include "CmdLineRunner.h"

using std::cout;
using std::endl;

#define TOOL_NAME "BlockDNFDKL"

double_t BlockDNFDKL::generateSample(){
	vector<int64_t> s = DS.randomAssignment();
	uint16_t Z1=1;
	for (int i =1;i<DS.randClsNum;i++){
		if (satisfies(s,i)){
			Z1=0;
			break;
		}
	}
	return Z1;
}

bool BlockDNFDKL::satisfies(vector<int64_t>& s, uint64_t clsnum){
	for(int i=0;i<F.clauses[clsnum].size();i++){
		int64_t curVar = F.clauses[clsnum][i];
		size_t varBlock = F.blocksByVar[curVar];
		int64_t chosenVar = s[varBlock];
		if(chosenVar != curVar)
			return false;
	}
	
	return true;
}

int BlockDNFDKL::solve(){
    if(DS.timedout())
        return -1;
	int retcode = AA();
    if(retcode<0) return retcode;
	result = DS.calculateFraction(count.num, count.den);
	return 0;
}

#ifdef ENABLE_MAINS
int main(int argc, char* argv[]){
	return cmdlineRun<BlockDNFDKL>(argc,argv,TOOL_NAME);
}	
#endif