#ifndef BLOCK_DNF_FORMULA_BUILDER_H
#define BLOCK_DNF_FORMULA_BUILDER_H

#include "BlockDNFFormula.h"

#include <unordered_set>
using namespace std;

class builder_exception : public exception{
public:
	builder_exception(const string& msg_) : exception(), msg(msg_) {}
	
	virtual const char* what() const throw(){
		return msg.c_str();
	}
	
private:
	string msg;
};


class BlockDNFFormulaBuilder{
public:
	BlockDNFFormulaBuilder(BlockDNFFormula& F_) : F(F_), maxVarClauses(0), maxVarBlocks(0), debugMode(false){
		F.blocks.clear();
		F.blocksByVar.clear();
		F.clauses.clear();
		F.clauses.push_back({});
		F.blocksByVar.push_back({});
	}
    
	void setDebugMode(bool debugMode=true){
        this->debugMode = debugMode;
    }
    
	BlockDNFFormulaBuilder& addClause(vector<int64_t>& clause) throw();
	BlockDNFFormulaBuilder& addBlock(vector<int64_t>& block) throw();
	void build() throw();
	
protected:
	BlockDNFFormula& F;
	
	vector<int64_t> blocksvars;
    vector<int64_t> varsSingletonBlock;
	vector<vector<int64_t>> clauses_temp;
	unordered_set<int64_t> clausevars;
	int64_t maxVarClauses;
    int64_t maxVarBlocks;
    bool debugMode;
private:
    int getNumMissingVariablesBefore(int64_t var);
    void optimize();
	bool noPartitionOrGap(){
		if(blocksvars[0]!=1)
			return true;

		for(size_t i=0;i<blocksvars.size()-1;i++)
			if(blocksvars[i] != blocksvars[i+1]-1)
				return true;

		return false;
	}
	
	bool isConflicting(vector<int64_t>& clause){
		for(size_t i=0;i<clause.size();i++){
			for(size_t j=i+1;j<clause.size();j++){
				auto curVar1 = clause[i];
				auto curVar2 = clause[j];
				if(F.blocksByVar[curVar1] == F.blocksByVar[curVar2] && curVar1!=curVar2)
					return true;
			}
		}
		
		return false;
	}
};
#endif