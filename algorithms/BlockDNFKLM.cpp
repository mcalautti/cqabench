#include "BlockDNFKLM.h"

#include <vector>
#include "adcs/Timers.h"
#include <sstream>
#include <iostream>

#include "CmdLineRunner.h"

using std::cout;
using std::endl;

#define TOOL_NAME "BlockDNFKLM"

inline uint64_t BlockDNFKLM::numberOfSets(){
	return F.m;
}

inline uint64_t BlockDNFKLM::generateIndex(){
	return rb.getRandInt(uid);
}

inline std::vector<int64_t> BlockDNFKLM::generateSample(){
	return DS.randomAssignment();
}

bool BlockDNFKLM::satisfies(vector<int64_t> s, uint64_t clsnum){
	for(int i=0;i<F.clauses[clsnum].size();i++){
		int64_t curVar = F.clauses[clsnum][i];
		size_t varBlock = F.blocksByVar[curVar];
		int64_t chosenVar = s[varBlock];
		if(chosenVar != curVar)
			return false;
	}
	
	return true;
}

int BlockDNFKLM::solve(){
    if(DS.timedout())
        return -1;
	int retcode = self_adjust_coverage_thm2();
    if (retcode<0) return retcode;
	result = DS.calculateFraction(count.num, count.den);
	return 0;
}

#ifdef ENABLE_MAINS
int main(int argc, char* argv[]){
	return cmdlineRun<BlockDNFKLM>(argc,argv,TOOL_NAME);
}	
#endif