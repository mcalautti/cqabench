#ifndef BLOCK_DNF_DKLM_H
#define BLOCK_DNF_DKLM_H

#include "adcs/DKL.h"
#include "BlockDNFFormula.h"
#include "BlockKLSampler.h"
#include <cmath>

class BlockDNFDKLM: public DKL{
	public:
		BlockDNFDKLM(BlockDNFFormula& F_, double_t eps_, double_t delta_,std::ostream& log_,double _timeStart, double _timeout):
		DKL(eps_, delta_, log_, _timeStart, _timeout), F(F_), DS(F_,_timeStart,_timeout){
			//DS = new BlockKLSampler(F);
		}
		int solve() override;
					
        std::string result;
        
	protected:
		double_t generateSample() override;
		bool satisfies(vector<int64_t>& s, uint64_t clsnum);
		
		BlockKLSampler DS;
		BlockDNFFormula& F;
};

#endif