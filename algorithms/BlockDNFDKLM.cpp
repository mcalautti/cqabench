#include <vector>

#include "BlockDNFDKLM.h"
#include "BlockDNFParser.h"
#include "adcs/Timers.h"
#include <sstream>

#include "CmdLineRunner.h"
using std::cout;
using std::endl;

#define TOOL_NAME "BlockDNFDKLM"

double_t BlockDNFDKLM::generateSample(){
	vector<int64_t> s = DS.randomAssignment();
	uint64_t Z1=0;
	for (int i =1;i<=F.m;i++){
		if (satisfies(s,i)){
			Z1++;
		}
	}
	return 1/(double_t)Z1;
}

bool BlockDNFDKLM::satisfies(vector<int64_t>& s, uint64_t clsnum){
	for(int i=0;i<F.clauses[clsnum].size();i++){
		int64_t curVar = F.clauses[clsnum][i];
		size_t varBlock = F.blocksByVar[curVar];
		int64_t chosenVar = s[varBlock];
		if(chosenVar != curVar)
			return false;
	}
	
	return true;
}

int BlockDNFDKLM::solve(){
    if(DS.timedout())
        return -1;
	int retcode = AA();
    if(retcode<0) return retcode;
	result = DS.calculateFraction(count.num, count.den);
	return 0;
}

#ifdef ENABLE_MAINS
int main(int argc, char* argv[]){
	return cmdlineRun<BlockDNFDKLM>(argc,argv,TOOL_NAME);
}	
#endif