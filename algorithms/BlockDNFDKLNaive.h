#ifndef BLOCK_DNF_DKL_NAIVE_H
#define BLOCK_DNF_DKL_NAIVE_H

#include "adcs/DKL.h"
#include "BlockDNFFormula.h"
#include "BlockNaiveSampler.h"
#include <cmath>

class BlockDNFDKLNaive: public DKL{
	public:
		BlockDNFDKLNaive(BlockDNFFormula& F_, double_t eps_, double_t delta_, std::ostream& log_, double _timeStart, double _timeout):
		DKL(eps_, delta_, log_, _timeStart, _timeout), F(F_), DS(F_,_timeStart,_timeout){
			//DS = new BlockNaiveSampler(F);
		}
		int solve() override;
				
        std::string result;
        
	protected:
		double_t generateSample() override;
		bool satisfies(vector<int64_t>& s);
		
		BlockNaiveSampler DS;
		BlockDNFFormula& F;
};

#endif