#ifndef KL_BLOCK_SAMPLER_H
#define KL_BLOCK_SAMPLER_H

#include <cstddef>
#include <gmp.h>
#include <gmpxx.h>
#include <ctime>
#include <iostream>
#include <vector>

#include "BlockSampler.h"
#include "BlockDNFFormula.h"
#include "adcs/RandomBits.h"

#include "adcs/Timers.h"
using std::vector;

class BlockKLSampler : public BlockSampler{
	public:
		BlockKLSampler(BlockDNFFormula& F_, double _timeStart, double _timeout):
		BlockSampler (F_, _timeStart, _timeout){
			rb.SeedEngine();
			sampling_array.resize(F.m+1);
			mpz_t temp;
			mpz_init(temp);
			mpz_init(totalAsnmt);
            mpz_init(U);
            //mpz_init(tempClsNum);
            //mpz_init(clsNum);
            gmp_randinit_mt (state1);
            mpz_init(randNum_clause);

			mpz_set_ui(totalAsnmt,1);
			
			bool doneComputingTotal = false;
			/* Marco: this part has changed to accomodate the blocks in the sampling probability.
			* With block dnf, we have to fill the sampling array as follows: position 0 contains how many assignments
			* satisfy clause 1, which is computed as the product of the size of all blocks 'not' in the clause (remember
			* we focus on positive dnf and that clauses are not contradicting). The next positions contain the above product
			* aggregated with the previous ones. In a nutshell, the sampling array is the probability distribution we among the clauses.
			*/
			for (int i = 1; i<= F.m;i++){
				
				// compute the product for the current clause
				mpz_set_ui(temp,1);
				for(size_t j=0;j<F.blocks.size();j++) {
					if(!doneComputingTotal)
						mpz_mul_ui(totalAsnmt,totalAsnmt,F.blocks[j].size());
						
					bool res = clauseHasBlock(i,j);
                    if(hasTimedout){
                        mpz_clear(temp);
                        return;
                    }
                    
                    if(!res)
						mpz_mul_ui(temp,temp,F.blocks[j].size());
                    
                    double elapsedTime = cpuTimeTotal() - _timeStart;
                    if(_timeout>0 && elapsedTime>_timeout){
                        hasTimedout = true;
                        mpz_clear(temp);
                        return;
                    }
				}
				
				doneComputingTotal=true;
				
				mpz_add(temp,sampling_array[i-1].get_mpz_t(),temp);
				mpz_set(sampling_array[i].get_mpz_t(),temp);
                
                double elapsedTime = cpuTimeTotal() - _timeStart;
                if(_timeout>0 && elapsedTime>_timeout){
                        hasTimedout = true;
                        mpz_clear(temp);
                        return;
                }
			}
			
			
			mpz_set(U,sampling_array[F.m].get_mpz_t());
			gmp_printf("U: %Zd\n",U);
			mpz_clear(temp);
	
			gmp_randseed_ui(state1, std::random_device{}());
			
			//mpz_init(clsNum);
			//mpz_init(tempClsNum);
		}
		
        virtual ~BlockKLSampler(){
            mpz_clear(U);
            mpz_clear(totalAsnmt);
            mpz_clear(randNum_clause);
            //mpz_clear(clsNum);
            //mpz_clear(tempClsNum);
            gmp_randclear(state1);
        }   
        
		vector<int64_t> randomAssignment() override;
		string calculateFraction(double_t num, uint64_t den) override;
		
		uint32_t randClsNum;

	protected:
		bool clauseHasBlock(int clause, size_t block){
			for(int i=0;i<F.clauses[clause].size();i++){
				int64_t var = F.clauses[clause][i];
				if(F.blocksByVar[var] == block)
					return true;
                double elapsedTime = cpuTimeTotal() - timeStart;
                if(timeout>0 && elapsedTime>timeout){
                        hasTimedout = true;
                        return true;
                }
			}
			return false;
		}
		
		vector<mpz_class> sampling_array;
		mpz_t U, totalAsnmt;
		mpz_t randNum_clause; 
		//mpz_t clsNum, tempClsNum;
		gmp_randstate_t state1;
		RandomBits rb;
};

#endif