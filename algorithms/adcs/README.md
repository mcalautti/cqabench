[![License: GPL3.0+](https://img.shields.io/badge/LICENSE-GPL3.0%2B-yellow.svg)](https://www.gnu.org/licenses/)

OVERVIEW
========

This is a slight modification of a subset of the suite of approximate counting algorithms for DNF formulas by [Shrotri](https://gitlab.com/Shrotri/DNF_Counting).  
This directory contains only the base approximation algorithms, which have been modified to be able to terminate execution after a given timeout.