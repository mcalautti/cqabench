/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#ifndef RANDOMBITS_H_
#define RANDOMBITS_H_

#include <string>
#include <random>
#include <vector>

using std::string;
using std::mt19937;
using std::mt19937_64;
using std::vector;

class RandomBits {
	private:
		string binary(unsigned x, uint32_t length);
		mt19937 randomEngine{};
		mt19937_64 randomEngine2{};
		
	public:
		void SeedEngine();
		void SeedEngine2();
		string GenerateRandomBits(uint32_t size);
		uint64_t getRandInt(std::uniform_int_distribution<uint64_t> uid);
		double_t getRandReal(std::uniform_real_distribution<double_t> urd);
		
		vector<size_t> GenerateRandomIndices(vector<vector<int64_t>>& blocks);
		vector<int64_t> GenerateRandomVariables(vector<vector<int64_t>>& blocks);
};

#endif
