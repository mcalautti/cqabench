/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#ifndef DNF_FORMULA_H
#define DNF_FORMULA_H

#include <vector>
#include <cstdint>

using std::vector;
class DNFFormula{
	public:
		DNFFormula(uint64_t n_, uint64_t m_, uint64_t minClauseSize_, uint64_t maxClauseSize_, vector<vector<int64_t>> clauses_)
		: n (n_), m(m_), minClauseSize(minClauseSize_), maxClauseSize(maxClauseSize_), clauses(clauses_) {}  
		
		uint64_t n, m, minClauseSize, maxClauseSize;
		vector<vector<int64_t>> clauses;
};

#endif
