/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 * Copyright (c) 2016, 2013, Supratik Chakraborty, Kuldeep S. Meel, Moshe Y. Vardi  
 * 
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#include "RandomBits.h"
#include <array>
#include <algorithm>
#include <utility>
#include <functional>

string RandomBits::binary(unsigned x, uint32_t length)
{
    uint32_t logSize = (x == 0 ? 1 : log2(x) + 1);
    string s;
    do {
        s.push_back('0' + (x & 1));
    } while (x >>= 1);
    for (uint32_t i = logSize; i < (uint32_t) length; i++) {
        s.push_back('0');
    }
    std::reverse(s.begin(), s.end());

    return s;

}

string RandomBits::GenerateRandomBits(uint32_t size)
{
    string randomBits;
    std::uniform_int_distribution<unsigned> uid {0, 2147483647U};
    uint32_t i = 0;
    while (i < size) {
        i += 31;
        randomBits += binary(uid(randomEngine), 31);
    }
    randomBits.erase(size);
    return randomBits;
}

void RandomBits::SeedEngine() {
	
    /* Initialize PRNG with seed from random_device */
    std::random_device rd{};
    std::array<int, 10> seedArray;
    std::generate_n(seedArray.data(), seedArray.size(), std::ref(rd));
    std::seed_seq seed(std::begin(seedArray), std::end(seedArray));
    randomEngine.seed(seed);
}

void RandomBits::SeedEngine2() {
	
    /* Initialize PRNG with seed from random_device */
    std::random_device rd{};
    std::array<int, 10> seedArray;
    std::generate_n(seedArray.data(), seedArray.size(), std::ref(rd));
    std::seed_seq seed(std::begin(seedArray), std::end(seedArray));
    randomEngine2.seed(seed);
}

uint64_t RandomBits::getRandInt(std::uniform_int_distribution<uint64_t> uid){
	return uid(randomEngine);
}

double_t RandomBits::getRandReal(std::uniform_real_distribution<double_t> urd){
	return urd(randomEngine2);
}

vector<size_t> RandomBits::GenerateRandomIndices(vector<vector<int64_t>>& blocks)
{
	vector<size_t> result(blocks.size());
	
	typedef std::uniform_int_distribution<size_t>::param_type param_type;
	
	//will change the lower/upper bounds at each iteration of the loop below.
	std::uniform_int_distribution<size_t> uid {0,10};
	
	for(size_t i = 0; i<blocks.size(); i++)
	{
		vector<int64_t>& block = blocks[i];
		size_t blSize = block.size();
		uid.param(param_type(0,blSize-1));
		result[i]=uid(randomEngine);
	}
	return result;
}

vector<int64_t> RandomBits::GenerateRandomVariables(vector<vector<int64_t>>& blocks)
{
	vector<int64_t> result(blocks.size());
	
	typedef std::uniform_int_distribution<size_t>::param_type param_type;
	
	//will change the lower/upper bounds at each iteration of the loop below.
	std::uniform_int_distribution<size_t> uid {0,10};
	
	for(size_t i = 0; i<blocks.size(); i++)
	{
		vector<int64_t>& block = blocks[i];
		size_t blSize = block.size();
		uid.param(param_type(0,blSize-1));
		result[i]=block[uid(randomEngine)];
	}
	return result;
}

