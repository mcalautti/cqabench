#ifndef SELF_ADJUST_H
#define SELF_ADJUST_H

#include <cmath>
#include <iostream>

struct SampleCount{
	uint64_t num;
	uint64_t den;
};

template<typename SetType>
class SelfAdjust{
	public:
		SelfAdjust(double_t eps_, double_t delta_, std::ostream& log_, double _timeStart, double _timeout):
		eps(eps_), delta(delta_), log_stream(log_), timeStart(_timeStart), timeout(_timeout){
			
		}

		virtual int solve() = 0;
				        
	protected:

		double_t eps, delta, mu, rho;
		SampleCount count;
		        
        std::ostream& log_stream;
        double timeStart;
        double timeout;

		virtual SetType generateSample() = 0;
		virtual uint64_t generateIndex() = 0;
		virtual uint64_t numberOfSets() = 0;
		virtual bool satisfies(SetType s, uint64_t index) = 0;

		int self_adjust_coverage_thm2(){
	
			uint64_t m = numberOfSets();
			uint64_t gtime = 0;
			uint64_t total = 0;
			uint64_t N_t = 0;
			uint64_t T = ceil(((8*(1+eps)*m*log(3/delta))/((1 - eps*eps/8)*eps*eps)));
			log_stream<<"T = "<<T<<endl;
			    
			//trial
			while(true){
				
				//cout<<"Getting random assnmnt"<<endl;
				auto s = generateSample();
				//cout<<"Got random assnmnt"<<endl;
				bool finish = false;
				//step:
				while(true){
					
					gtime ++;
					if(gtime > T){
						finish = true;
						break;
					}
					uint64_t j = generateIndex();
					if (satisfies(s, j)){
						break;
					}
		            double elapsedTime = cpuTimeTotal() - timeStart;
		            if(timeout>0 && elapsedTime>timeout)
		                return -1;
				}
				if(finish){
					break;
				}
				
				total = gtime;
				N_t ++;
			}
			//handle multiplication by U separately in calling function
			log_stream<<" N_t = "<<N_t<<endl;
			log_stream<<"Num = "<<total<<" Den = "<<m*N_t<<endl;
			log_stream<<"mu = "<<(double_t)total/(m*N_t)<<endl;
			count.num = total;
			count.den = m*N_t;
		    
		    return 1;
		}
};

#endif