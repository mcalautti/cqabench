#ifndef BLOCK_DNF_ADDITIVE_MC_H
#define BLOCK_DNF_ADDITIVE_MC_H

#include "AdditiveMC.h"
#include "BlockDNFFormula.h"
#include "BlockNaiveSampler.h"

class BlockDNFAdditiveMC: public AdditiveMC{
	public:
		BlockDNFAdditiveMC(BlockDNFFormula& F_, double_t eps_, double_t delta_, std::ostream& log_, double _timeStart, double _timeout):
		AdditiveMC(eps_, delta_, log_,_timeStart,_timeout), F(F_), DS(F_,_timeStart,_timeout){
			//DS = new BlockNaiveSampler(F);
		}
		
		int solve() override;
				
        std::string result;

	protected:
		double_t generateSample() override;
		bool satisfies(vector<int64_t>& s);
		
		BlockNaiveSampler DS;
		BlockDNFFormula& F;
        
};

#endif