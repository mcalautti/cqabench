#ifndef CMD_LINE_RUNNER_H
#define CMD_LINE_RUNNER_H

#include <string>
#include <iostream>
#include <fstream>
#include <csignal>

#include "archive_util.h"

using namespace std;

static string _tool_name;
static vector<string> _comments;
static double _eps, _delta;
static int _tryNum;

static void sendCsvRow(int tryNum,const string& tool_name,
                const vector<string>& data, double eps, double delta,
                double elapsedTime, const string& result) {
    
    cerr<<tryNum<<";"<<tool_name<<";";
    
    for(auto value : data)
        cerr<<value<<";";
    
    cerr<<eps<<";"<<delta<<";"<<elapsedTime<<";"<<result<<endl;
}

template <typename Algorithm>
static int cmdlineRun(int argc, char* argv[], const char* tool_name){
    if (argc!=6){
		cout<<"Usage: "<<tool_name<<" <eps> <delta> <input_bdnf_dimacs_tar.gz> <try_num> <timeout (sec)>"<<endl;
		return 0;
	}
    
    _tool_name = tool_name;
    
	double_t eps = strtod(argv[1],NULL);
    _eps = eps;

	double_t delta = strtod(argv[2],NULL);
    _delta = delta;
    
	std::string bdnfFile = std::string(argv[3]);
    
    int tryNum = std::stoi(std::string(argv[4]));
    _tryNum = tryNum;
    
    double timeout = std::stod(std::string(argv[5]));
    
    cout<<tool_name<<" invoked with epsilon="<<eps<<" delta="<<delta<<" dnfFile="<<bdnfFile<<" try_num="<<tryNum<<" timeout="<<timeout<<endl<<endl;
	
    BlockDNFParser P;
    
    //do not optimize the formula again, we rely on the shape of the input file.
    P.setDebugMode(true);
        
    ifstream ifs(bdnfFile, ios::binary);

    ifs.seekg(0,ios::end);
    size_t length = ifs.tellg();
    
    char *buff = new char[length];
    ifs.seekg(0, ios::beg);
    ifs.read(buff, length);

    ifs.close();
    
    archive* in_archive = openMemoryArchive(buff,length);
    
    string bdnfFileName, bdnfContent;
    
    double globalTimeStart = cpuTimeTotal();
    
    while(nextArchiveEntryString(in_archive,bdnfFileName,bdnfContent)){
        BlockDNFFormula F;
        std::vector<std::string> comment;
        istringstream iss(bdnfContent);
        
        if(!P.parse(iss,F,comment)){
            cerr<<"Could not parse dimacs file "<<bdnfFileName<<endl;
            exit(1);
        }
        
        //escape the ; in comment
        vector<string> newComments;
        for(auto& value : comment){
            size_t pos = 0;
            while ((pos = value.find(";")) != std::string::npos) {
                value.replace(pos, 1, "_");
            }
            value = value.substr(value.find(':')+2);
            newComments.push_back(value);
        }
        newComments.push_back(std::to_string(F.n));
        newComments.push_back(std::to_string(F.m));
        newComments.push_back(std::to_string(F.numBlockConstraints));
        newComments.push_back(std::to_string(F.minClauseSize));
        newComments.push_back(std::to_string(F.maxClauseSize));
        newComments.push_back(std::to_string(F.blocks.size()));
        newComments.push_back(std::to_string(F.minBlockSize));
        newComments.push_back(std::to_string(F.maxBlockSize));

        _comments = newComments;
        
        double startTime = cpuTimeTotal();

        Algorithm D(F,eps,delta,cout,globalTimeStart,timeout);
        
        cout<<"starting counting"<<endl;
        int retcode = D.solve();
        cout<<"finished counting"<<endl;

        double elapsedTime = cpuTimeTotal() - startTime;
        
        std::string res = retcode<0? "timeout" : D.result;
        
        cout<<"Final result: "<<res<<endl;
        cout<<tool_name<<" took "<<elapsedTime<<" seconds"<<endl;

        //Print experiment data
        if(retcode<0){
            sendCsvRow(tryNum,tool_name,newComments,eps,delta,elapsedTime,"-1.0");
            break;
        }else
            sendCsvRow(tryNum,tool_name,newComments,eps,delta,elapsedTime,D.result);
    }

    closeReadArchive(in_archive);
    delete[] buff;
    
    return 0;
}

#endif