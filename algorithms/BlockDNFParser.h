#ifndef BLOCK_DNF_PARSER_H
#define BLOCK_DNF_PARSER_H

#include "BlockDNFFormula.h"
#include <string>
#include <ostream>
#include <istream>

using namespace std;

/**
 * @class BlockDNFParser
 * @author cqadev
 * @date 04/05/19
 * @file BlockDNFParser.h
 * @brief This class is responsible to serialize a block dnf to file. The special encoding used allows for the same file to be both
 * parsed as both a dnf with additional block information, and a cnf whose number of satisfying assignment correspond to
 * the block assignments *not* satisfying the original dnf.
 * In particular, the way such a file is encoded implies that each satisfying assignment of the encoded cnf, corresponds, by
 * flipping each truth value, to an unsatisfying block assignment of the corresponding block dnf.
 */
class BlockDNFParser{
public:

    BlockDNFParser() : debugMode(false) {}
    
	bool parse(const string& bdnf_path, BlockDNFFormula& F) throw();
	bool parse(const string& bdnf_path, BlockDNFFormula& F, vector<string>& comments) throw();
	
    bool parse(istream& in_stream, BlockDNFFormula& F) throw();
    bool parse(istream& in_stream, BlockDNFFormula& F, vector<string>& comments) throw();

	bool saveToFile(const string& cnf_out_path, const BlockDNFFormula& F, const std::initializer_list<string>& comments);

	bool saveToFile(const string& cnf_out_path, const BlockDNFFormula& F, const vector<string>& comments);
	bool saveToFile(const string& cnf_out_path, const BlockDNFFormula& F, const string& comment);

    bool saveToStream(ostream& out_stream, const BlockDNFFormula& F, const std::initializer_list<string>& comments);

	bool saveToStream(ostream& out_stream, const BlockDNFFormula& F, const vector<string>& comments);
	bool saveToStream(ostream& out_stream, const BlockDNFFormula& F, const string& comment);
    
    void setDebugMode(bool debugMode = true){ this->debugMode = debugMode; }
protected:
    bool debugMode;
};
#endif