#include "AdditiveMC.h"

#include <cmath>
#include <iostream>

#include "adcs/Timers.h"
using namespace std;

int AdditiveMC::AdditiveAlgorithm(){
	
	int N = (int)ceil(log(2 / delta) / (2 * epsilon * epsilon));
	
	log_stream<<"Performing N="<<N<<" iterations"<<endl;
	
	double_t S = 0;
	    
	for(size_t i=0;i<N;i++){
		S+=generateSample();
        double elapsedTime = cpuTimeTotal() - timeStart;
        if(timeout>0 && elapsedTime>timeout)
            return -1;
    }
	
	log_stream<<"Num: "<<S<<endl;
	
	count.num = S;
	count.den = N;
	
	return 1;
}