#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <cstring>
#include <chrono>
#include <postgresql/libpq-fe.h>
#include <jsoncpp/json/json.h>
#include <regex>

#include "./lib/BlockDNFFormula.h"
#include "./lib/BlockDNFFormulaBuilder.h"
#include "./lib/BlockDNFParser.h"
#include "archive_util.h"

using namespace std;
using namespace std::chrono;

/*string readQuery(string path){
    string res;
    ifstream t(path);
    stringstream buffer;
    buffer << t.rdbuf();
    res = buffer.str();
    return res;
}*/
enum class LogType{ LOG, ERROR };

void printLog(const std::string& message, const std::string& queryName, const std::string& queryPath,const std::string& connectionPath, double elapsedTime, bool debugMode, LogType type){
    std::string type_str = type == LogType::LOG? "LOG" : "ERROR";
    std::string debug_str = debugMode? "NO_OPTIMIZATION" : "OPTIMIZED";

    cerr<<"dnf-rewriter"<<";"<<type_str<<";"<<connectionPath<<";"<<queryName<<";"<<queryPath<<";"<<elapsedTime<<";"<<debug_str<<";"<<message<<endl;
}

void evaluateQuery(PGconn *conn, string queryString, string schemaName, string connectionPath, string queryName, string queryPath, string outPath, bool debugMode, vector<string> comment_header, bool constructDNF);

int main(int argc, char **argv){
    
    if(argc != 4 && argc != 5 && argc != 6)   {
        cout<<"Usage: connection_json_path query_path out_path [-disable_opt (disable dnf gen. optimizations)] [-original_query (run original query only)]"<<endl;
        exit(-1);
    }

    string connectionPath = string(argv[1]);
    string queryPath = string(argv[2]);
    string outPath = string(argv[3]);
    
    bool debugMode = false;
    bool runOriginal = false;
    if(argc==5 && string(argv[4]) == "-disable_opt"){
        debugMode = true;
    }else if(argc==5 && string(argv[4]) == "-original_query"){
    	runOriginal = true;
    }else if(argc==6 && string(argv[4]) == "-disable_opt" && string(argv[5]) == "-original_query"){
    	debugMode = true;
    	runOriginal = true;
    }else if(argc>=5){
        cout<<"Invalid parameter: "<<argv[4]<<endl;
        exit(-1);
    }

    ifstream ifsConn(connectionPath);
    Json::Reader reader;
    Json::Value connInfo;   
    try{
        reader.parse(ifsConn, connInfo);
    }
    catch(...){
        printLog("Unable to parse connection file","",queryPath,connectionPath,0,debugMode,LogType::ERROR);
        exit(-1);
    }
    ifsConn.close();

    ifstream ifsQ(queryPath);
    Json::Value queryBatch;   
    try{
        reader.parse(ifsQ, queryBatch);
    }
    catch(...){
        printLog("Unable to parse QUERY file","",queryPath,connectionPath,0,debugMode,LogType::ERROR);
        exit(-1);
    }
    ifsQ.close();

    const string connString = "dbname=\'" + connInfo["dbname"].asString() 
                + "\' user=\'" + connInfo["user"].asString() 
                + "\' host=\'" + connInfo["host"].asString() 
                + "\' password=\'" + connInfo["password"].asString() + "\'";
 
    /* Make a connection to the database */
    PGconn     *conn = PQconnectdb(connString.c_str());

    /* Check to see that the backend connection was successfully made */
    if (PQstatus(conn) != CONNECTION_OK)
    {
        printLog("Unable to connect to database","",queryPath,connectionPath,0,debugMode,LogType::ERROR);
       
        PQfinish(conn);
        exit(1);
   }

   cout<<"Query batch: "<<queryBatch["name"]<<endl;
   cout<<"----"<<endl;
   for (int i = 0; i < queryBatch["experiments"].size(); i++){
        cout<<"----"<<endl;
        cout<<"Experiment: "<<queryBatch["experiments"][i]["name"].asString()<<endl;
        if(queryBatch["experiments"][i].isMember("skip") and queryBatch["experiments"][i]["skip"].asString() == "true"){
            cout<<"Skipping..."<<endl;
            cout<<"++++"<<endl;
            continue;
        }

        string schemaName = queryBatch["experiments"][i]["schema"].asString() + "_"+ queryBatch["experiments"][i]["name"].asString();
        
        if(queryBatch["experiments"][i].isMember("experiment_schema")){
            schemaName = queryBatch["experiments"][i]["experiment_schema"].asString();
            cout<<"Overriding experiment schema name: "<<schemaName<<endl;
        }

        string queryName = queryBatch["experiments"][i]["query"].asString();
        
        Json::Value *queryJSON = nullptr;
        for(int j = 0; j < queryBatch["queries"].size(); j++){
            if(queryBatch["queries"][j]["name"].asString() == queryName)
            {
                queryJSON = &queryBatch["queries"][j];
                break;
            }
        }
        if(queryJSON == nullptr){
            cout<<"Cannot find query "<< queryName;
            exit(-1);
        }

        string queryString = (*queryJSON)["rew"].asString();
        string queryOriginalSQL = (*queryJSON)["original"].asString();
        string queryIsSafe = (*queryJSON)["isSafe"].asString();
        cout<<"Schema: "<<schemaName<<endl;
        cout<<"Query: "<<queryName<<endl;
        cout<<"\t Original: "<<queryOriginalSQL<<endl;
        cout<<"\t Is safe: "<<queryIsSafe<<endl;
        cout<<"\t Rewrite: "<<queryString<<endl;

        
        high_resolution_clock::time_point t1 = high_resolution_clock::now();

        //Comment header. Information here is the same through all the tuples
        vector<string> comment_header;
        comment_header.push_back("Query name: " + queryName);
        comment_header.push_back("Is safe: " + queryIsSafe);
        comment_header.push_back("Experiment: "+ queryBatch["experiments"][i]["name"].asString());
        
        evaluateQuery(conn, runOriginal? queryOriginalSQL : queryString, schemaName, connectionPath, queryName, queryPath, outPath, debugMode, comment_header, !runOriginal);
        high_resolution_clock::time_point t2 = high_resolution_clock::now();
        duration<double, std::ratio<1,1>> time_span = t2 - t1;
        cout<<"\t Done! "<<time_span.count()<<"s"<<endl;
        cout<<"----"<<endl;
    }
   PQfinish(conn);
    
   return 0;
}

void evaluateQuery(PGconn *conn, 
	string queryString, 
	string schemaName, 
	string connectionPath, 
	string queryName, 
	string queryPath, 
	string outPath, 
	bool debugMode,
	vector<string> comment_header, bool constructDNF){

    high_resolution_clock::time_point t1 = high_resolution_clock::now();

    string schemaQuery = "SET search_path = " + schemaName;
    
    PQexec(conn, schemaQuery.c_str());

    if(!constructDNF){
    	//check if the query is boolean, and if so, let postgres run it as boolean
    	std::regex bool_regex(R"((S|s)(E|e)(L|l)(E|e)(C|c)(T|t)\s+\*)");
    	if(std::regex_search(queryString,bool_regex)){
    		//check if query ends with ;, and replace in case
    		size_t index = queryString.find(";",0);
    		if(index != std::string::npos)
    			queryString[index]=' ';
    		
    		queryString = "SELECT 1 WHERE EXISTS ("+queryString+")";
    	}
    }
    PGresult *res = PQexec(conn, queryString.c_str());
    
    if(!constructDNF){
    	high_resolution_clock::time_point t2 = high_resolution_clock::now();
    	duration<double, std::ratio<1,1>> time_span = t2 - t1;
    	cout<<"\t Original Query execution time:  "<<time_span.count()<<"s"<<endl;
    	double evalTimeMs = time_span.count();
    	printLog("Original Query execution time",queryName,queryPath,connectionPath,time_span.count(),debugMode,LogType::LOG);
    	PQclear(res);
    	return;
    }

    string outFileName = outPath+ "/" + queryName + ".tar.gz";
	archive* out_archive = openWriteArchive(outFileName.c_str());

    int nFields = PQnfields(res); //Number of attributes in the answer
    int nTuples = PQntuples(res); //Number of tuples in the answer
 

    int metaDataPositions = -1;
    for (int i = 0; i < nFields; i++){
        if(strcmp("_table", PQfname(res, i)) == 0){
            metaDataPositions = i;
            break;
        }
     }
    if(metaDataPositions == -1){
    	string pgstatus(PQresStatus(PQresultStatus(res)));
        printLog("Unable to find metadata in the answer. PGresultStatus: " + pgstatus,queryName,queryPath,connectionPath,0,debugMode,LogType::ERROR);
        cout<<"Postgres error message: "<<PQerrorMessage(conn)<<endl;
        exit(-1);
    }

    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double, std::ratio<1,1>> time_span = t2 - t1;
    cout<<"\t Query execution time:  "<<time_span.count()<<"s"<<endl;
    double evalTimeMs = time_span.count();
    printLog("Query execution time",queryName,queryPath,connectionPath,time_span.count(),debugMode,LogType::LOG);

    int i = 0;
    int dnfCount = 0;
    BlockDNFParser parser;
    while(i < nTuples){   
    	int nHom = 0; // Number of homomorphisms for the current tuple

        t1 = high_resolution_clock::now();

        map<string, map<int, int>> blockBeginByName;
        int nextAvailable = 1;
        
        BlockDNFFormula f;
        
        BlockDNFFormulaBuilder builder(f);
        builder.setDebugMode(debugMode);

        vector<string> blockTuple;
        for(int j = 0; j < metaDataPositions; j++){
            string s(PQgetvalue(res, i, j));
            blockTuple.push_back(s);
        }

        while(i < nTuples){

            vector<string> currentTuple;    
            for(int j = 0; j < metaDataPositions; j++){
                string s(PQgetvalue(res, i, j));
        
                currentTuple.push_back(s);
            }

            if(currentTuple != blockTuple)
                break;

			nHom++; //This is a new homomorphism for the current tuple

            vector<int64_t> newClause;
            for(int j = metaDataPositions; j < nFields; j+=4){
                string tabName = PQgetvalue(res, i, j);
                int bid = atoi(PQgetvalue(res, i, j+1));
                int tid = atoi(PQgetvalue(res, i, j+2));
                int bsize = atoi(PQgetvalue(res, i, j+3));
                
               
    
                if((blockBeginByName[tabName]).find(bid) == blockBeginByName[tabName].end()){
                    (blockBeginByName[tabName])[bid] = nextAvailable;
                    vector<int64_t> newBlock;
                    
                    for(int k = 0; k < bsize; k++){
                        newBlock.push_back(nextAvailable+k);
                    }
                    builder.addBlock(newBlock);
                    nextAvailable += bsize;
                }
                newClause.push_back(blockBeginByName[tabName][bid] + (tid-1));
            }
            builder.addClause(newClause);
            i++;
        }
        
        builder.build();

        //exclude dnfs with zero ratio
        if(f.m == 0)
        	continue;

        string tuple = "Tuple: [";
        for(int k = 0; k < blockTuple.size(); k++){
            tuple += blockTuple[k];
            if(k < blockTuple.size()-1) 
                tuple += ", "; 
        }

        tuple += ']';

        //Preparing comment
        vector<string> comment(comment_header);
        comment.push_back("Total tuples: " + to_string(nTuples));
		comment.push_back("Evaluation time in ms: " + to_string(evalTimeMs));
		comment.push_back(tuple);
        comment.push_back("Hom. for this tuple: " + to_string(nHom));
        


        string dnfName = queryName + "_" + to_string(dnfCount);

        ostringstream outStream;
        parser.saveToStream(outStream, f, comment);
        
        string entryFileName = dnfName +".txt";

        string dnfContent = outStream.str();
        addFileToArchive(out_archive,dnfContent.data(),dnfContent.size(), entryFileName.c_str());

        dnfCount++;

        t2 = high_resolution_clock::now();
        time_span = t2 - t1;

        cout<<"\t Construction time of DNF "<<dnfName<<": "<<time_span.count()<<"s"<<endl;

        printLog("Construction time of DNF",dnfName,queryPath,connectionPath,time_span.count(),debugMode,LogType::LOG);
        
        }

    closeWriteArchive(out_archive);
    PQclear(res);
}