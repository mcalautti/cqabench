#pragma once

#include <archive.h>
#include <archive_entry.h>

archive* openWriteArchive(const char* outfile){
	archive* a = archive_write_new();
  	archive_write_add_filter_gzip(a);
  	archive_write_set_format_pax_restricted(a); 
  	archive_write_open_filename(a, outfile);
  	return a;
}

void addFileToArchive(archive* arc, const char* data, size_t size, const char* filename){
	archive_entry* entry = archive_entry_new(); 
    archive_entry_set_pathname(entry, filename);
    archive_entry_set_size(entry, size);
    archive_entry_set_filetype(entry, AE_IFREG);
    archive_entry_set_perm(entry, 0644);
    archive_write_header(arc, entry);
	archive_write_data(arc, data, size);
    archive_entry_free(entry);
}

void closeWriteArchive(archive* arc){
	archive_write_close(arc);
 	archive_write_free(arc);
}