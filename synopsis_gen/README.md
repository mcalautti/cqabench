[![License: GPL3.0+](https://img.shields.io/badge/LICENSE-GPL3.0%2B-yellow.svg)](https://www.gnu.org/licenses/)

OVERVIEW
========

Arguments:
* json connection file
* json file with queries
* output directory

Description: this tool will construct, for each query in the input json file, a tar.gz file containing all Block DNF formulas, each corresponding to an output tuple of the query. This tar.gz is essentially the set of all synopses of the database w.r.t. to all the tuples with non-zero relative frequency.


REQUIREMENTS:
=============

[libarchive 3.3.3](https://libarchive.org/)  
[jsoncpp 1.7.4](https://github.com/open-source-parsers/jsoncpp)  
[libpq 11.5](https://www.postgresql.org/docs/current/libpq.html)

INSTALLATION
==============

If the dependencies above are properly installed, running `make` will suffice to build the tool. 
The binary will be found in the bin directory.

HOW TO USE
==============

Running the binary without arguments will show the help. The binary takes as input a json file specifying the connection to a Postgres instance, a json file collecting queries (together with their rewriting and information on the noisy database to use), and the path of the output directory. The json with the queries is likely the one obtained after executing "rewrite_queries.py" and "set_noise_parameters.py" over the json file with plain conjunctive queries. Moreover, noisy databases constructed using the resulting json file are assumed to have been generated with "noise_gen.py".
