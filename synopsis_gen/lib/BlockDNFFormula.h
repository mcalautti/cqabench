#ifndef BLOCK_DNF_FORMULA_H
#define BLOCK_DNF_FORMULA_H

#include "adcs/DNFFormula.h"
#include <cstdint>
/**
 * @class BlockDNFFormula
 * @author cqadev
 * @date 02/05/19
 * @file BlockDNFFormula.h
 * @brief In a BlockDNFFormula, variables are all positive, and are partitioned in blocks.
 * Variables and clauses are numbered from 1.
 * Blocks are numbered from 0.
 * The number of variables n denotes ALL variables, also the ones not appearing in the formula, but in the blocks.
 */
class BlockDNFFormula : public DNFFormula
{
public:
	BlockDNFFormula(uint64_t n_, uint64_t m_, uint64_t minClauseSize_, uint64_t maxClauseSize_,
					vector<vector<int64_t>> clauses_, vector<vector<int64_t>> blocks_, vector<size_t> blByVar_):
					DNFFormula(n_,m_,minClauseSize_,maxClauseSize_,clauses_),
					blocks(blocks_),
					blocksByVar(blByVar_), maxBlockSize(0), minBlockSize(0), numBlockConstraints(0) {}

	BlockDNFFormula() : DNFFormula(0,0,0,0,{}){}
				
/**
	position i in "blocks" is the set of variables in block i.
*/
	vector<vector<int64_t>> blocks;
	
/**
	position i in blocksByVar is the block id of variable i
*/
	vector<size_t> blocksByVar;
    
    size_t maxBlockSize, minBlockSize, numBlockConstraints;
};
#endif