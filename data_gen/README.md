[![License: GPL3.0+](https://img.shields.io/badge/LICENSE-GPL3.0%2B-yellow.svg)](https://www.gnu.org/licenses/)

OVERVIEW
========

This directory contains the following scripts:

tpc_gen.py
=
Arguments: a data generation configuration file (json)  
Description: It generates a database, according to the configuration given in the json file, using one of the supported TPC datagen tools (i.e., tpch and tpcds).


REQUIREMENTS (pip):
=============
None.

HOW TO USE
==============
Running the script without arguments will show the help.  
The given json configuration file specifies which kind of (consistent) database to construct (i.e., tpch or tpcds), the scale factor, and in which directory to output the sql files.
The output directory will contain the .sql file to be imported in postgres, and the schema of the database, in json format (to be used with the other tools in this repository).  
The resulting .sql files can be imported in PostgreSQL with the command:  
```psql -f filename.sql```  
Example configuration files can be found in the `configs` directory.
