#!/usr/bin/env python3

import os, sys, codecs, re, subprocess, shutil, json, importlib
from cqarewriting import viewsGenerator
from cqarewriting import constants

create_table_regex = r"create table (.+)\n\(\n((?:.*?\,\n)+)\s+primary key \((.*)\)\n\);"

def get_schema_content(benchmark,tools_dir):
    if benchmark=="tpc-ds":
        filename = os.path.join(tools_dir,"tpcds.sql")
        with codecs.open(filename,"r","utf-8") as f:
            data = f.read()
    elif benchmark=="tpc-h":
        filename = os.path.join(tools_dir,"tpch.sql")
        with codecs.open(filename,"r","utf-8") as f:
            data = f.read()
    else:
        raise Exception("Unrecognized benchmark "+benchmark)

    return data

def run_dbgen(benchmark,tools_dir,scale):
    cur_dir = os.getcwd()
    if benchmark=="tpc-ds":
        os.chdir(tools_dir)
        ret = subprocess.call(["./dsdgen","-FORCE", "Y", "-SCALE",str(scale),"-TERMINATE","N","-DIR",cur_dir])
        os.chdir(cur_dir)
        return ret
    elif benchmark=="tpc-h":
        os.chdir(tools_dir)
        ret = subprocess.call(["./dbgen","-fv","-s",str(scale)])
        subprocess.call(["sed -i 's/|$//' *.tbl"],shell=True)
        subprocess.call(['for f in *.tbl; do mv -- "$f" '+cur_dir+'/"${f%.tbl}.dat"; done'],shell=True)
        os.chdir(cur_dir)
        return ret
    else:
        raise Exception("Unrecognized benchmark "+benchmark)
    

def get_schema_info(schema_sql):
    matches = re.findall(create_table_regex,schema_sql, re.IGNORECASE)
    tables = []
    for m in matches:
        name = m[0].strip()
        attributes = [ a.strip().split(' ')[0] for a in m[1].split('\n') if a.strip()]
        key = [a.strip() for a in m[2].split(',')]
        tables.append({'name':name,'attributes':attributes,'key':key})
    return tables

def get_schema_preamble(dbname, schema_name):
    preamble = "DROP DATABASE IF EXISTS "+dbname+";\n"
    preamble += "CREATE DATABASE "+dbname+";\n"
    preamble += "\\connect "+dbname+";\n\n"
    preamble += "SET search_path = "+schema_name+";\n\n"

    return preamble

def get_schema_data_copy(schema_info):
    data_copy = ""
    for info in schema_info:
        data_copy += "\\copy "+info['name']+" FROM '"+info['name']+".dat' DELIMITER '|' CSV ENCODING 'LATIN1' NULL '';\n"

    return data_copy
    
def get_schema_views(schema_info, schema_name, materialize):
    schema = {"tables" : schema_info, "name": schema_name }

    vg = viewsGenerator.viewsGenerator(schema)

    createStatement = 'CREATE VIEW '
    if materialize:
	    createStatement = 'CREATE MATERIALIZED VIEW '

    view_text = "\n\n"
    for tab_name in vg.tableNames:
        q = vg.getView(tab_name)

        view_text += "DROP VIEW IF EXISTS " +  vg.schemaName +"." + tab_name + constants.VIEW_SUFFIX+";\n"
        view_text += createStatement + vg.schemaName +"." +tab_name + "_blocks AS " +q+";\n\n"

    return view_text

if len(sys.argv) != 2:
    print("Usage: config_file.json")
    sys.exit(0)

config = None
with codecs.open(sys.argv[1],"r","utf-8") as f:
    config = json.load(f)

benchmark = config['benchmark']
tools_dir = config['tools_dir']
scale = config['scale']
dbname = config['dbname']
schema_name = 'public'
if 'db_schema_name' in config:
    schema_name = config['db_schema_name']
output_dir = config['output_dir']
materialize_views = config['materialize_views']
schema_only = False
if "schema_only" in config:
    schema_only = config['schema_only']

print("Opening schema...")
schema_sql = get_schema_content(benchmark,tools_dir)

schema_info = get_schema_info(schema_sql)

if not os.path.exists(output_dir):
    os.makedirs(output_dir)

if not schema_only:
    print("Generating data with given scale...")
    ret = run_dbgen(benchmark,tools_dir,scale)
    if ret != 0:
        print("Error while generating data!")
        sys.exit(-1)

    #move files to output directory
    ret = subprocess.call(["mv *.dat "+output_dir],shell=True)
    if ret != 0:
        print("Could not move dat files to directory "+output_dir)
        sys.exit(-1)

#writing down final sql file
print("Generating final sql file...")
output_file = os.path.join(output_dir,dbname+".sql")

with codecs.open(output_file,"w","utf-8") as f:
    preamble = get_schema_preamble(dbname,schema_name)
    f.write(preamble)

    f.write(schema_sql)

    views = get_schema_views(schema_info,schema_name,materialize_views)
    f.write(views)

    data_copy = get_schema_data_copy(schema_info)
    f.write(data_copy)

json_schema_file = os.path.join(output_dir,dbname+".json")
with codecs.open(json_schema_file,"w","utf-8") as f:
    schema = {"tables" : schema_info, "name": schema_name}
    json.dump(schema,f,indent=4)

print("Done.")